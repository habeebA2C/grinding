using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Grinding.Repository;
using Grinding.Services;

namespace Grinding
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the Pallet.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(30);//You can set Time   
            });
            services.AddDbContext<DBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionStr")));
            services.AddDbContext<DBContext2>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionStr2")));
            services.AddDbContext<DBContext3>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionStr3")));
            services.AddControllersWithViews();
            services.AddTransient<IUser, UserRepository>();
            services.AddTransient<IRole, RoleRepository>();
            services.AddTransient<IDepartment, DepartmentRepository>();
            services.AddTransient<IWorkflow, WorkflowRepository>();
            services.AddTransient<IWorkflowDetail, WorkflowDetailRepository>();
            services.AddTransient<IWorkflowTracker,WorkflowTrackerRepository>();
            services.AddTransient<IDocuments, DocumentsRepository>();
            services.AddTransient<IRoleMenu, RoleMenuRepository>();
            services.AddTransient<IMenu, MenuRepository>();
            services.AddTransient<IUserDepartment, UserDeapatmentRepository>();
            services.AddTransient<IInsight, InsightRepository>();
            services.AddTransient<IPallet, PalletRepository>();
            services.AddTransient<ICategory, CategoryRepository>();
            services.AddTransient<IPalletDetail, ContainerDetailRepository>();
            services.AddTransient<IWorkTime, WorkTimeRepository>();
            services.AddTransient<ICarryAllDetail, CarryAllDetailRepository>();
            services.AddTransient<IPalletNumber, PalletNumberRepository>();
            services.AddTransient<IPendingPart, PendingPartRepository>();
            services.AddTransient<ICategoryDetail, CategoryDetailRepository>();
            services.AddTransient<IStep, StepRepository>();
            services.AddTransient<IQualityCheck, QualityCheckRepository>();
            services.AddTransient<IReason, ReasonRepository>();
            services.AddTransient<IPaintColor, PaintColorRepository>();
            services.AddTransient<ISerialDetail, SerialDetailRespository>();
            services.AddTransient<IMakeModelPart, MakeModelPartRepository>();
            services.AddTransient<IMasterData, MasterDataRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Index}/{id?}");
            });
        }
    }
}
