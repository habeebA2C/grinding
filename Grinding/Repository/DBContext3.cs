﻿using Microsoft.EntityFrameworkCore;
using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class DBContext3 : DbContext
    {

        public DBContext3(DbContextOptions<DBContext3> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<PendingPart>(eb =>
                {
                    eb.HasNoKey();
                    eb.ToTable("PendingPart");
                    eb.Property(v => v.PartId).HasColumnName("PartId");
                });
        }
        public DbSet<PendingPart> PendingPart { get; set; }
    }
}
