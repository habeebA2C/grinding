﻿using Microsoft.EntityFrameworkCore.Internal;
using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class RoleRepository : IRole
    {
        private readonly DBContext db;

        public RoleRepository(DBContext _db)
        {
            db = _db;
        }

        public IEnumerable<Role> GetRoles => from u in db.Roles
                                             join r in db.Users
                                               on u.r_cre_by equals r.u_id
                                             select new Role
                                             {
                                                 r_id = u.r_id,
                                                 r_description = u.r_description,
                                                 r_active_yn = u.r_active_yn,
                                                 r_cre_by = u.r_cre_by,
                                                 r_cre_by_name = r.u_name + '-' + r.u_full_name,
                                                 r_cre_date = u.r_cre_date

                                             };

        public void Add(Role role)
        {
            db.Roles.Add(role);
            db.SaveChanges();
        }

        public Role GetRole(int id)
        {
            Role role = db.Roles.Find(id);
            return role;
        }

        public void Remove(int id)
        {
            Role role = db.Roles.Find(id);
            db.Roles.Remove(role);
            db.SaveChanges();
        }

        public void Update(Role role1)
        {
            db.Update(role1);
            db.SaveChanges();
        }
    }
}
