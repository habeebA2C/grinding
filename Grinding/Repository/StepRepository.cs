﻿using Microsoft.EntityFrameworkCore.Internal;
using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class StepRepository : IStep
    {
        private readonly DBContext db;

        public StepRepository(DBContext _db)
        {
            db = _db;
        }

        public IEnumerable<Step> GetSteps => from u in db.Steps
                                             join r in db.Users
                                               on u.s_cre_by equals r.u_id
                                             select new Step
                                             {
                                                 s_id = u.s_id,
                                                 s_description = u.s_description,
                                                 s_active_yn = u.s_active_yn,
                                                 s_cre_by = u.s_cre_by,
                                                 s_cre_by_name = r.u_name + '-' + r.u_full_name,
                                                 s_cre_date = u.s_cre_date

                                             };

        public void Add(Step step)
        {
            db.Steps.Add(step);
            db.SaveChanges();
        }

        public Step GetStep(int id)
        {
            Step step = db.Steps.Find(id);
            return step;
        }

        public Step getStepByName(string s_description)
        {
            var quary = (from u in db.Steps
                        where u.s_description == s_description
                        select new Step
                        {
                            s_id = u.s_id,
                            s_description = u.s_description,
                            s_active_yn = u.s_active_yn,
                            s_cre_by = u.s_cre_by,
                            s_cre_date = u.s_cre_date

                        }).FirstOrDefault();
            return quary;
        }

        public IEnumerable<Step> GetStepsByWorkflow(int workflow )
        {
            var quary = from u in db.Steps
                        join r in db.WorkflowDetails
                          on u.s_id equals r.wd_step_id
                        where r.wd_workflow_id == workflow
                        orderby r.wd_priority ascending
                        select new Step
                        {
                            s_id = u.s_id,
                            s_description = u.s_description,
                            s_active_yn = u.s_active_yn,
                            s_cre_by = u.s_cre_by,
                            s_cre_date=u.s_cre_date
            
                        };
            return quary;
        }

        public void Remove(int id)
        {
            Step step = db.Steps.Find(id);
            db.Steps.Remove(step);
            db.SaveChanges();
        }

        public void Update(Step step)
        {
            db.Steps.Update(step);
            db.SaveChanges();
        }
    }
}
