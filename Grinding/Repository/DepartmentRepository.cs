﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class DepartmentRepository : IDepartment
    {
        private DBContext db;

        public DepartmentRepository(DBContext _db)
        {
            db = _db;
        }

        public IEnumerable<Department> GetDepartments => from d in db.Departments
                                                         join w in  db.Workflows  on d.d_workflow equals w.w_id
                                                         join f in db.Users on d.d_cre_by equals f.u_id
                                                         select new Department
                                                         { d_id=d.d_id,
                                                           d_description=d.d_description,
                                                           d_workflow=d.d_workflow,
                                                           d_workflow_name=w.w_description,
                                                           d_active_yn=d.d_active_yn,
                                                           d_cre_by=d.d_cre_by,
                                                           d_cre_by_name = f.u_name + '-' + f.u_full_name,
                                                           d_cre_date =d.d_cre_date
                                                         };

        public void Add(Department department)
        {
            db.Departments.Add(department);
            db.SaveChanges();
        }

        public Department GetDepartment(int id)
        {
            Department department = db.Departments.Find(id);
            return department;
        }

        public void Remove(int id)
        {
            Department department = db.Departments.Find(id);
            db.Departments.Remove(department);
            db.SaveChanges();
           
        }

        public void Update(Department department)
        {
            db.Update(department);
            db.SaveChanges();
        }
    }
}
