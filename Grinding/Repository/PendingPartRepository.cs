﻿using Grinding.Models;
using Grinding.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class PendingPartRepository : IPendingPart
    {
        private readonly DBContext3 db;

        public PendingPartRepository(DBContext3 _db)
        {
            db = _db;
        }
        public IEnumerable<PendingPart> GetPendingParts => db.PendingPart;

        public PendingPart GetPendingPart(int id)
        {
            PendingPart pendingPart = db.PendingPart.Find(id);
            return pendingPart;
        }
        public IEnumerable<PendingPart> GetParts()
        {
                IEnumerable<PendingPart> pendingParts = Enumerable.Empty<PendingPart>();
                pendingParts = db
                  .PendingPart
                  .FromSqlRaw("EXECUTE spGetParts")
                  .ToList();
                return pendingParts;
        }
  }
}
