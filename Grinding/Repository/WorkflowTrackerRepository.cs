﻿using Grinding.Models;
using Grinding.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class WorkflowTrackerRepository : IWorkflowTracker
    {
        private DBContext db;

        public WorkflowTrackerRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<WorkflowTracker> GetWorkflowTrackers => db.workflowTrackers;

        public void Add(WorkflowTracker workflowTracker)
        {
            db.workflowTrackers.Add(workflowTracker);
            db.SaveChanges();
        }

        public int getNextPriorityByWorkflowAndSerialNumber(int workflow, string serialNo)
        {
            int priority = 0;
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec getNextPriorityByWorkflowAndSerialNumber  @workflow='" + workflow+"',@serialNo='"+ serialNo + "'";


                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            priority = int.Parse(reader["nextPriority"].ToString());
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
              
            }
            finally
            {
                conn.Close();
            }
          
            return priority;
        }

        public WorkflowTracker GetWorkflowTracker(int id)
        {
            WorkflowTracker workflowTracker = db.workflowTrackers.Find(id);
            return workflowTracker;
        }

        public IEnumerable<WorkflowTracker> GetWorkflowTrackersbySerialNo(string serialNo)
        {
            var query = from u in db.workflowTrackers
                        join r in db.Departments on u.wt_dep_id equals r.d_id
                        join j in db.Departments on u.wt_dep_to equals j.d_id
                        join k in db.Users on u.wt_cre_by equals k.u_id
                        where u.wt_serial_no == serialNo
                        orderby u.wt_cre_date descending
                        select new WorkflowTracker
                        {
                            wt_id = u.wt_id,
                            wt_pall_desc = u.wt_pall_desc,
                            wt_con_code = u.wt_con_code,
                            wt_dep_id = u.wt_dep_id,
                            wt_dep_description = r.d_description,
                            wt_dep_to = u.wt_dep_to,
                            wt_dep_to_description = j.d_description,
                            wt_workflow_id = u.wt_workflow_id,
                            wt_workflow_to = u.wt_workflow_to,
                            wt_serial_no = u.wt_serial_no,
                            wt_cat_id = u.wt_cat_id,
                            wt_step_id = u.wt_step_id,
                            wt_step_description = u.wt_step_description,
                            wt_step_to = u.wt_step_to,
                            wt_step_to_description = u.wt_step_to_description,
                            wt_status = u.wt_status,
                            wt_status_to = u.wt_status_to,
                            wt_approve_status = u.wt_approve_status,
                            wt_cre_by = u.wt_cre_by,
                            wt_cre_by_name = k.u_name + " " + k.u_full_name,
                            wt_cre_date = u.wt_cre_date
                        };
            return query;
        }

        public void Remove(int id)
        {
            WorkflowTracker workflowTracker = db.workflowTrackers.Find(id);
            db.workflowTrackers.Remove(workflowTracker);
        }
    }
}
