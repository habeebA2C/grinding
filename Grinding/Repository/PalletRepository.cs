﻿using Grinding.Models;
using Grinding.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class PalletRepository : IPallet
    {
        private DBContext db;

        public PalletRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<Pallet> GetPallets => from u in db.Pallets
                                                       join d in db.Users
                                                        on u.con_cre_by equals d.u_id
                                                       select new Pallet
                                                       {
                                                           con_id = u.con_id,
                                                           con_code = u.con_code,
                                                           con_dep_id = u.con_dep_id,
                                                           con_cat_id = u.con_cat_id,
                                                           con_allocated_by = u.con_allocated_by,
                                                           con_allocated_to = u.con_allocated_to,
                                                           con_cre_by = u.con_cre_by,
                                                           con_cre_by_name = d.u_name + '-' + d.u_full_name,
                                                           con_cre_date = u.con_cre_date

                                                       };

        //public Result Add(Pallet Pallet)
        //{
        //    Result result = new Result();
        //    var conn = db.Database.GetDbConnection();
        //    try
        //    {
        //        conn.Open();
        //        using (var command = conn.CreateCommand())
        //        {
        //            string query = @"exec createContainer " +
        //            "@con_cat_id = '" + Pallet.con_cat_id + "',@con_cre_by ='" + Pallet.con_cre_by + "'";
        //            command.CommandText = query;

        //            DbDataReader reader = command.ExecuteReader();
        //            var dataTable = new DataTable();

        //            if (reader.HasRows)
        //            {
        //                dataTable.Load(reader);
        //                result.Objects = JsonConvert.SerializeObject(dataTable);
        //                result.Message = "Success";
        //            }

        //            reader.Dispose();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Message = ex.Message + " " + ex.InnerException;
        //    }
        //    return result;
        //}

        public Result Add(Pallet pallet)
        {
            Result result = new Result();
           
            try
            {
              
                db.Pallets.Add(pallet);
                db.SaveChanges();
                result.Objects = pallet.con_code;
                result.Message = "Success";
            }
            catch (Exception ex)
            {
                result.Message = ex.Message + " " + ex.InnerException;
            }
            
            return result;
        }

        public IEnumerable<Pallet> AverageTimeTakenBySearch(DateTime from, DateTime to, int con_cat_id, int con_allocated_by)
        {
            List<Pallet> result = new List<Pallet>();
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {

                    string query = @"select con.*,[dbo].[getUserName](con_allocated_to) con_allocated_to_Name,[dbo].[getWorkingTimeByContainer](con_code) con_workedTime,cat_description con_cat_description, 
                            [dbo].[getUserName](con_cre_by) con_cre_by_name,
                            [dbo].[getPalletNumberName](con_pall_id) con_pall_description,
                            [dbo].[getContainerDetailCountByContainer](con_code) con_cond_count
                            from  Containers(nolock) con
                                    left join Categories on con_cat_id=cat_id where 1=1 ";


                    if (!from.ToString().Equals("1/1/0001 12:00:00 AM") && !to.ToString().Equals("1/1/0001 12:00:00 AM"))
                        query += " and con_cre_date between '" + from + @"' and '" + to + @"' ";

                 

                    if (con_allocated_by != 0)
                        query += " and con_allocated_by='" + con_allocated_by + @"'";

                    command.CommandText = query;
                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Pallet Pallet = new Pallet();
                            Pallet.con_id = Int32.Parse(reader["con_id"].ToString());
                            Pallet.con_code = reader["con_code"].ToString();
                            if (!reader["con_dep_id"].ToString().Equals("")) Pallet.con_dep_id = Int32.Parse(reader["con_dep_id"].ToString());
                            if (!reader["con_doc_id"].ToString().Equals("")) Pallet.con_doc_id = Int32.Parse(reader["con_doc_id"].ToString());
                            if (!reader["con_workflow_id"].ToString().Equals("")) Pallet.con_workflow_id = Int32.Parse(reader["con_workflow_id"].ToString());
                            if (!reader["con_allocated_to"].ToString().Equals("")) Pallet.con_allocated_to = int.Parse(reader["con_allocated_to"].ToString());
                            if (!reader["con_cond_count"].ToString().Equals("")) Pallet.con_cond_count = int.Parse(reader["con_cond_count"].ToString());
                            if (!reader["con_allocated_to_Name"].ToString().Equals("")) Pallet.con_allocated_to_Name = reader["con_allocated_to_Name"].ToString();
                            if (!reader["con_workedTime"].ToString().Equals(""))
                            {
                                Pallet.con_workedTime = int.Parse(reader["con_workedTime"].ToString());
                            }
                            if (!reader["con_cat_description"].ToString().Equals("")) Pallet.con_cat_description = reader["con_cat_description"].ToString();
                            if (!reader["con_pall_description"].ToString().Equals("")) Pallet.con_pall_description = reader["con_pall_description"].ToString();
                            Pallet.con_cat_id = int.Parse(reader["con_cat_id"].ToString());
                            Pallet.con_finished_yn = reader["con_finished_yn"].ToString();
                            Pallet.con_cre_by_name = reader["con_cre_by_name"].ToString();
                            Pallet.con_cre_date = DateTime.Parse(reader["con_cre_date"].ToString());

                            result.Add(Pallet);
                        }
                    }
                    reader.Dispose();

                }
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public IEnumerable<Pallet> PalletReportBySearch(DateTime from, DateTime to, int con_cre_by, int con_pall_id, int con_cat_id,int con_allocated_to,string Type)
        {
            List<Pallet> result = new List<Pallet>();
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {

                    string query = @"select con.*,[dbo].[getUserName](con_allocated_to) con_allocated_to_Name,[dbo].[getWorkingTimeByContainer](con_code) con_workedTime,cat_description con_cat_description, 
                            [dbo].[getUserName](con_cre_by) con_cre_by_name,
                            [dbo].[getPalletNumberName](con_pall_id) con_pall_description,
                            [dbo].[getContainerDetailCountByContainer](con_code) con_cond_count,
                            [dbo].getStepsByPallet(con_code) con_steps,
                            [dbo].[getDepartmentName] (con_dep_id) con_dep_desciption
                            from  Pallets(nolock) con
                            left join Categories on con_cat_id=cat_id where 1=1 ";


                    if (!from.ToString().Equals("1/1/0001 12:00:00 AM") && !to.ToString().Equals("1/1/0001 12:00:00 AM"))
                        query += " and con_cre_date between '" + from + @"' and '" + to + @"' ";

                    if (con_pall_id != 0) query += " and con_pall_id='" + con_pall_id + @"'";

                    if (con_cat_id != 0) query += " and con_cat_id='" + con_cat_id + @"'";

                    if (con_cre_by != 0)
                        query += " and con_cre_by='" + con_cre_by + @"'";

                    if (con_allocated_to != 0)
                        query += " and con_allocated_by='" + con_allocated_to + @"'";

                    if(Type!=null)
                    {
                        if (Type.Equals("Unallocated"))
                            query += " and con_allocated_by is null";
                        if (Type.Equals("Allocated"))
                            query += " and con_allocated_by is not null";
                    }
                   

                    query += " order by con_id desc";

                    command.CommandText = query;
                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Pallet Pallet = new Pallet();
                            Pallet.con_id = Int32.Parse(reader["con_id"].ToString());
                            Pallet.con_code = reader["con_code"].ToString();
                            if (!reader["con_dep_id"].ToString().Equals("")) Pallet.con_dep_id = int.Parse(reader["con_dep_id"].ToString());
                            if (!reader["con_dep_desciption"].ToString().Equals("")) Pallet.con_dep_desciption = reader["con_dep_desciption"].ToString();
                            if (!reader["con_doc_id"].ToString().Equals("")) Pallet.con_doc_id = Int32.Parse(reader["con_doc_id"].ToString());
                            if (!reader["con_workflow_id"].ToString().Equals(""))  Pallet.con_workflow_id = Int32.Parse(reader["con_workflow_id"].ToString());
                            if (!reader["con_allocated_to"].ToString().Equals(""))  Pallet.con_allocated_to = int.Parse(reader["con_allocated_to"].ToString());
                            if (!reader["con_cond_count"].ToString().Equals(""))  Pallet.con_cond_count = int.Parse(reader["con_cond_count"].ToString());
                            if (!reader["con_allocated_to_Name"].ToString().Equals("")) Pallet.con_allocated_to_Name = reader["con_allocated_to_Name"].ToString();
                            if (!reader["con_workedTime"].ToString().Equals(""))
                            {
                                Pallet.con_workedTime = int.Parse(reader["con_workedTime"].ToString());
                            }
                            if (!reader["con_steps"].ToString().Equals("")) Pallet.con_steps = reader["con_steps"].ToString();
                            if (!reader["con_cat_description"].ToString().Equals("") ) Pallet.con_cat_description = reader["con_cat_description"].ToString();
                            if (!reader["con_pall_description"].ToString().Equals("") ) Pallet.con_pall_description = reader["con_pall_description"].ToString();
                            Pallet.con_cat_id = int.Parse(reader["con_cat_id"].ToString());
                            Pallet.con_finished_yn = reader["con_finished_yn"].ToString();
                            Pallet.con_cre_by_name = reader["con_cre_by_name"].ToString();
                            Pallet.con_cre_date = DateTime.Parse(reader["con_cre_date"].ToString());

                            result.Add(Pallet);
                        }
                    }
                    reader.Dispose();

                }
            }
            finally
            {
                conn.Close();
            }
            return result;

        }

        public Pallet GetPallet(int id)
        {
            Pallet Pallet = db.Pallets.Find(id);
            return Pallet;
        }

        public Pallet GetPalletByCode(string con_code)
        {
            var query = from u in db.Pallets
                        join d in db.Users
                         on u.con_cre_by equals d.u_id
                        join j in db.PalletNumbers
                        on u.con_pall_id equals j.pn_id
                        where u.con_code == con_code
                        select new Pallet
                        {
                            con_id = u.con_id,
                            con_code = u.con_code,
                            con_dep_id = u.con_dep_id,
                            con_pall_description = j.pn_description,
                            con_cat_id = u.con_cat_id,
                            con_pall_id = u.con_pall_id,
                            con_allocated_by = u.con_allocated_by,
                            con_allocated_to = u.con_allocated_to,
                            con_cre_by = u.con_cre_by,
                            con_cre_by_name = d.u_name + '-' + d.u_full_name,
                            con_cre_date = u.con_cre_date

                        };
            return query.FirstOrDefault();
        }

        public IEnumerable<Pallet> GetPalletBySearch(DateTime from,DateTime to,int con_cre_by, int con_pall_id, int con_cat_id)
        {
            var query = from u in db.Pallets
                        join d in db.Users
                         on u.con_cre_by equals d.u_id
                        join j in db.PalletNumbers
                        on u.con_pall_id equals j.pn_id
                        join k in db.Categories
                        on u.con_cat_id equals k.cat_id
                        select new Pallet
                        {
                            con_id = u.con_id,
                            con_code = u.con_code,
                            con_dep_id = u.con_dep_id,
                            con_cat_id = u.con_cat_id,
                            con_cat_description = k.cat_description,
                            con_pall_id = u.con_pall_id,
                            con_pall_description = j.pn_description,
                            con_allocated_by = u.con_allocated_by,
                            con_allocated_to = u.con_allocated_to,
                            con_cre_by = u.con_cre_by,
                            con_cre_by_name = d.u_name + '-' + d.u_full_name,
                            con_cre_date = u.con_cre_date

                        };

            if (con_pall_id != 0)
                query = query.Where(x => x.con_pall_id == con_pall_id);
            if (con_cre_by != 0)
                query = query.Where(x => x.con_cre_by == con_cre_by);
            if (con_cat_id != 0)
                query = query.Where(x => x.con_cat_id == con_cat_id);
            if (!from.ToString().Equals("1/1/0001 12:00:00 AM") && !to.ToString().Equals("1/1/0001 12:00:00 AM"))
                query = query.Where(x => x.con_cre_date >= from && x.con_cre_date <= to);

            return query;
        }

        public IEnumerable<Pallet> getMyPallets(int u_id)
        {
            var query = from u in db.Pallets
                        join d in db.Users
                         on u.con_cre_by equals d.u_id
                         join k in db.PalletNumbers 
                         on u.con_pall_id equals k.pn_id
                        where u.con_allocated_to == u_id &&u.con_finished_yn!="Y"
                        select new Pallet
                        {
                            con_id = u.con_id,
                            con_dep_id = u.con_dep_id,
                            con_code_desc =k.pn_description + '-' + u.con_code,
                            con_code = u.con_code,
                            con_cat_id = u.con_cat_id,
                            con_allocated_by = u.con_allocated_by,
                            con_allocated_to = u.con_allocated_to,
                            con_cre_by = u.con_cre_by,
                            con_cre_by_name = d.u_name + '-' + d.u_full_name,
                            con_cre_date = u.con_cre_date

                        };
            return query;
        }

        public void Remove(int id)
        {
            Pallet Pallet = db.Pallets.Find(id);
            db.Pallets.Remove(Pallet);
            db.SaveChanges();
        }

        public void Update(Pallet Pallet)
        {
            db.Pallets.Update(Pallet);
            db.SaveChanges();
        }

        public DataTable WorkAllocationBySearch(DateTime from, DateTime to,int dep_id)
        {
             DataTable dt = new DataTable();
                var conn = db.Database.GetDbConnection();
            try
            {

                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec WorkAllocationReport @from = '" + from + "',@to ='" + to + "',@dep_id='"+dep_id+"'";
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        dt.Load(reader);
                    }
                    reader.Dispose();
                }
            }catch(Exception ex)
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }

            return dt;
        }
     
        public IEnumerable<Pallet> ActivePalletsBySearch(int con_cre_by, int con_pall_id, int con_cat_id, int con_allocated_to)
        {
            List<Pallet> result = new List<Pallet>();
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {

                    string query = @"select con.*,[dbo].[getUserName](con_allocated_to) con_allocated_to_Name,[dbo].[getWorkingTimeByContainer](con_code) con_workedTime,cat_description con_cat_description, 
                            [dbo].[getUserName](con_cre_by) con_cre_by_name,
                            [dbo].[getPalletNumberName](con_pall_id) con_pall_description,
                            [dbo].[getContainerDetailCountByContainer](con_code) con_cond_count,
                            [dbo].getStepsByPallet(con_code) con_steps
                            from  Pallets(nolock) con
                            left join Categories on con_cat_id=cat_id where 1=1 and  isnull(con_finished_yn,'N')='N' ";



                    if (con_pall_id != 0) query += " and con_pall_id='" + con_pall_id + @"'";

                    if (con_cat_id != 0) query += " and con_cat_id='" + con_cat_id + @"'";

                    if (con_cre_by != 0)
                        query += " and con_cre_by='" + con_cre_by + @"'";

                    if (con_allocated_to != 0)
                        query += " and con_allocated_to='" + con_allocated_to + @"'";

                    command.CommandText = query;
                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Pallet Pallet = new Pallet();
                            Pallet.con_id = Int32.Parse(reader["con_id"].ToString());
                            Pallet.con_code = reader["con_code"].ToString();
                            if (!reader["con_doc_id"].ToString().Equals("")) Pallet.con_doc_id = Int32.Parse(reader["con_doc_id"].ToString());
                            if (!reader["con_dep_id"].ToString().Equals("")) Pallet.con_dep_id = Int32.Parse(reader["con_dep_id"].ToString());
                            if (!reader["con_workflow_id"].ToString().Equals("")) Pallet.con_workflow_id = Int32.Parse(reader["con_workflow_id"].ToString());
                            if (!reader["con_allocated_to"].ToString().Equals("")) Pallet.con_allocated_to = int.Parse(reader["con_allocated_to"].ToString());
                            if (!reader["con_cond_count"].ToString().Equals("")) Pallet.con_cond_count = int.Parse(reader["con_cond_count"].ToString());
                            if (!reader["con_allocated_to_Name"].ToString().Equals("")) Pallet.con_allocated_to_Name = reader["con_allocated_to_Name"].ToString();
                            if (!reader["con_workedTime"].ToString().Equals(""))
                            {
                                Pallet.con_workedTime = int.Parse(reader["con_workedTime"].ToString());
                            }
                            if (!reader["con_cat_description"].ToString().Equals("")) Pallet.con_cat_description = reader["con_cat_description"].ToString();
                            if (!reader["con_pall_description"].ToString().Equals("")) Pallet.con_pall_description = reader["con_pall_description"].ToString();
                            if (!reader["con_steps"].ToString().Equals("")) Pallet.con_steps = reader["con_steps"].ToString();
                            Pallet.con_cat_id = int.Parse(reader["con_cat_id"].ToString());
                            Pallet.con_finished_yn = reader["con_finished_yn"].ToString();
                            Pallet.con_cre_by_name = reader["con_cre_by_name"].ToString();
                            Pallet.con_cre_date = DateTime.Parse(reader["con_cre_date"].ToString());

                            result.Add(Pallet);
                        }
                    }
                    reader.Dispose();

                }
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public IEnumerable<Pallet> findPallet(string serialNo, int step, int part, int make, int model, string type)
        {
            List<Pallet> pallets = new List<Pallet>();
            DataTable dt = new DataTable();
            var conn = db.Database.GetDbConnection();
            try
            {
             

                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec findPallet @serialNo = '" + serialNo + "',@step ='" + step + "'" +
                        ",@part ='" + part + "',@make ='" + make + "',@model ='" + model + "'";
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Pallet Pallet = new Pallet();
                            Pallet.con_id = Int32.Parse(reader["con_id"].ToString());
                            Pallet.con_code = reader["con_code"].ToString();
                            if (!reader["con_dep_id"].ToString().Equals("")) Pallet.con_dep_id = Int32.Parse(reader["con_dep_id"].ToString());
                            if (!reader["con_doc_id"].ToString().Equals("")) Pallet.con_doc_id = Int32.Parse(reader["con_doc_id"].ToString());
                            if (!reader["con_workflow_id"].ToString().Equals("")) Pallet.con_workflow_id = Int32.Parse(reader["con_workflow_id"].ToString());
                            if (!reader["con_allocated_to"].ToString().Equals("")) Pallet.con_allocated_to = int.Parse(reader["con_allocated_to"].ToString());
                            if (!reader["con_cond_count"].ToString().Equals("")) Pallet.con_cond_count = int.Parse(reader["con_cond_count"].ToString());
                            if (!reader["con_allocated_to_Name"].ToString().Equals("")) Pallet.con_allocated_to_Name = reader["con_allocated_to_Name"].ToString();
                            if (!reader["con_workedTime"].ToString().Equals(""))
                            {
                                Pallet.con_workedTime = int.Parse(reader["con_workedTime"].ToString());
                            }
                            if (!reader["con_cat_description"].ToString().Equals("")) Pallet.con_cat_description = reader["con_cat_description"].ToString();
                            if (!reader["con_pall_description"].ToString().Equals("")) Pallet.con_pall_description = reader["con_pall_description"].ToString();
                            if (!reader["con_steps"].ToString().Equals("")) Pallet.con_steps = reader["con_steps"].ToString();
                            Pallet.con_cat_id = int.Parse(reader["con_cat_id"].ToString());
                            Pallet.con_finished_yn = reader["con_finished_yn"].ToString();
                            Pallet.con_cre_by_name = reader["con_cre_by_name"].ToString();
                            Pallet.con_cre_date = DateTime.Parse(reader["con_cre_date"].ToString());

                            pallets.Add(Pallet);
                        }
                    }
                    reader.Dispose();
                }
            }catch(Exception ex)
            {
                Trace.WriteLine(ex.Message + " " + ex.InnerException);
                conn.Close();
            }
            finally
            {
                conn.Close();
            }

            return pallets;
        }
    }
}
