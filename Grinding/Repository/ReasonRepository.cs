﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class ReasonRepository : IReason
    {
        private readonly DBContext db;

        public ReasonRepository(DBContext _db)
        {
            db = _db;
        }

        public IEnumerable<Reason> GetReasons => from u in db.Reasons
                                                 join d in db.Users
                                                  on u.rs_cre_by equals d.u_id
                                                 select new Reason
                                                 {
                                                     rs_id = u.rs_id,
                                                     rs_description = u.rs_description,
                                                     rs_type = u.rs_type,
                                                     rs_cre_by = u.rs_cre_by,
                                                     rs_cre_by_name = d.u_name + '-' + d.u_full_name,
                                                     rs_cre_date = u.rs_cre_date

                                                 };

        public void Add(Reason reason)
        {
            db.Reasons.Add(reason);
            db.SaveChanges();
        }

        public Reason GetReason(int id)
        {
            Reason reason = db.Reasons.Find(id);
            return reason;
        }

        public void Remove(int id)
        {
            Reason reason = db.Reasons.Find(id);
            db.Reasons.Remove(reason);
            db.SaveChanges();
        }

        public void Update(Reason reason)
        {
            db.Reasons.Update(reason);
            db.SaveChanges();
        }
    }
}
