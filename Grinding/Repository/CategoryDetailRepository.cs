﻿using Grinding.Models;
using Grinding.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class CategoryDetailRepository : ICategoryDetail
    {
        private DBContext db;

        public CategoryDetailRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<CategoryDetail> GetCategoryDetails()
        {
            int catd_cat_id = 0;
            List<CategoryDetail> categoryDetails = new List<CategoryDetail>();
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec getCategoryDetails   @catd_cat_id = '" + catd_cat_id + "',@Type='All'";


                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            CategoryDetail categoryDetail = new CategoryDetail();
                            categoryDetail.catd_id = int.Parse(reader["catd_id"].ToString());
                            categoryDetail.catd_cat_id = int.Parse(reader["catd_cat_id"].ToString());
                            categoryDetail.catd_part = int.Parse(reader["catd_part"].ToString());
                            categoryDetail.catd_Make = int.Parse(reader["catd_Make"].ToString());
                            categoryDetail.catd_Model = int.Parse(reader["catd_Model"].ToString());
                            categoryDetail.catd_Step = int.Parse(reader["catd_Step"].ToString());
                            categoryDetail.catd_cre_by = int.Parse(reader["catd_cre_by"].ToString());
                            categoryDetail.catd_cre_date = DateTime.Parse(reader["catd_cre_date"].ToString());
                            categoryDetail.catd_part_Name = reader["catd_part_Name"].ToString();
                            categoryDetail.catd_Make_Name = reader["catd_Make_Name"].ToString();
                            categoryDetail.catd_Model_Name = reader["catd_Model_Name"].ToString();
                            categoryDetail.catd_cat_description = reader["catd_cat_description"].ToString();
                            categoryDetail.catd_cre_by_name = reader["catd_cre_by_name"].ToString();
                            categoryDetail.catd_Step_Name = reader["catd_Step_Name"].ToString();
                            categoryDetails.Add(categoryDetail);
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }

           
            return categoryDetails;
        }

     

        public Result Add(CategoryDetail categoryDetail)
        {
           
            Result result = new Result();
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec addCategoryDetail  " +
                    " @catd_cat_id = '" + categoryDetail.catd_cat_id + "',@catd_Make ='" + categoryDetail.catd_Make + "',@catd_Model ='" + 
                    categoryDetail.catd_Model + "',@catd_part ='" + categoryDetail.catd_part + "',@catd_step ='" + categoryDetail.catd_Step + "',@catd_cre_by='" + categoryDetail.catd_cre_by + "'";

                  
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result.Message = reader[0].ToString();
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message + " " + ex.InnerException;
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public CategoryDetail GetCategoryDetail(int id)
        {

            CategoryDetail categoryDetail = new CategoryDetail();
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec getCategoryDetails   @catd_cat_id = '" + id + "',@Type=''";


                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            categoryDetail.catd_id = int.Parse(reader["catd_id"].ToString());
                            categoryDetail.catd_cat_id = int.Parse(reader["catd_cat_id"].ToString());
                            categoryDetail.catd_part = int.Parse(reader["catd_part"].ToString());
                            categoryDetail.catd_Make = int.Parse(reader["catd_Make"].ToString());
                            categoryDetail.catd_Model = int.Parse(reader["catd_Model"].ToString());
                            categoryDetail.catd_Step = int.Parse(reader["catd_Step"].ToString());
                            categoryDetail.catd_cre_by = int.Parse(reader["catd_cre_by"].ToString());
                            categoryDetail.catd_cre_date = DateTime.Parse(reader["catd_cre_date"].ToString());
                            categoryDetail.catd_part_Name = reader["catd_part_Name"].ToString();
                            categoryDetail.catd_Make_Name = reader["catd_Make_Name"].ToString();
                            categoryDetail.catd_Model_Name = reader["catd_Model_Name"].ToString();
                            categoryDetail.catd_cat_description = reader["catd_cat_description"].ToString();
                            categoryDetail.catd_cre_by_name = reader["catd_cre_by_name"].ToString();
                            categoryDetail.catd_Step_Name = reader["catd_Step_Name"].ToString();
                         
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                conn.Close();
            }
            return categoryDetail;
            //CategoryDetail categoryDetail = new CategoryDetail();
            //categoryDetail = db
            //  .CategoryDetails
            //  .FromSqlRaw("EXECUTE getCategoryDetails {0},{1}", id,"Id")
            //  .ToList().FirstOrDefault();
            //return categoryDetail;
        }

        public IEnumerable<CategoryDetail> GeCategoryDetailsByCategory(int catd_cat_id)
        {
            List<CategoryDetail> categoryDetails = new List<CategoryDetail>();
            var conn = db.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec getCategoryDetails   @catd_cat_id = '" + catd_cat_id + "',@Type='Category'";


                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            CategoryDetail categoryDetail = new CategoryDetail();
                            categoryDetail.catd_id = int.Parse(reader["catd_id"].ToString());
                            categoryDetail.catd_cat_id = int.Parse(reader["catd_cat_id"].ToString());
                            categoryDetail.catd_part = int.Parse(reader["catd_part"].ToString());
                            categoryDetail.catd_Make = int.Parse(reader["catd_Make"].ToString());
                            categoryDetail.catd_Model = int.Parse(reader["catd_Model"].ToString());
                            categoryDetail.catd_Step = int.Parse(reader["catd_Step"].ToString());
                            categoryDetail.catd_cre_by = int.Parse(reader["catd_cre_by"].ToString());
                            categoryDetail.catd_cre_date = DateTime.Parse(reader["catd_cre_date"].ToString());
                            categoryDetail.catd_part_Name = reader["catd_part_Name"].ToString();
                            categoryDetail.catd_Make_Name = reader["catd_Make_Name"].ToString();
                            categoryDetail.catd_Model_Name = reader["catd_Model_Name"].ToString();
                            categoryDetail.catd_cat_description = reader["catd_cat_description"].ToString();
                            categoryDetail.catd_cre_by_name = reader["catd_cre_by_name"].ToString();
                            categoryDetail.catd_Step_Name = reader["catd_Step_Name"].ToString();
                            categoryDetails.Add(categoryDetail);
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
         
            return categoryDetails;
        }

        public void Remove(int id)
        {
            CategoryDetail categoryDetail = db.CategoryDetails.Find(id);
            db.CategoryDetails.Remove(categoryDetail);
            db.SaveChanges();
        }

        public void Update(CategoryDetail categoryDetail)
        {
            db.CategoryDetails.Update(categoryDetail);
            db.SaveChanges();
        }
    }
}
