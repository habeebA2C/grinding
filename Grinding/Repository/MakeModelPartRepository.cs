﻿using Grinding.Models;
using Grinding.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class MakeModelPartRepository : IMakeModelPart
    {
        private DBContext db;

        public MakeModelPartRepository(DBContext _db)
        {
            db = _db;
        }
        public Result Add(MakeModelPart makeModelPart)
        {
            Result result = new Result();
            db.MakeModelParts.Add(makeModelPart);
            try
            {
                db.SaveChanges();
                result.Message = "Success";
            }
           
            catch (DbUpdateException ex)
            {
                SqlException innerException = ex.InnerException.InnerException as SqlException;
                if (innerException != null && (innerException.Number == 2627 || innerException.Number == 2601))
                {
                    result.Message = "All ready Exist !!";

                }
                else
                {
                    result.Message = "All ready Exists !!!";
                }
            }
            return result;
        }
        public MakeModelPart GetMakeModelPart(int id)
        {
            MakeModelPart makeModelPart = db.MakeModelParts.Find(id);
            return makeModelPart;
        }

        public MakeModelPart getMakeModelPartByMakeModelPart(int? cond_Make, int? cond_Model, int? cond_part)
        {
            MakeModelPart makeModelPart = new MakeModelPart();

            var conn = db.Database.GetDbConnection();

            conn.Open();
            try
            {
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec getMakeModelPartByAll @mmp_Make='" + cond_Make + "',@mmp_Model='" + cond_Model + "',@mmp_part='" + cond_part + "'";
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            makeModelPart.mmp_id = Int32.Parse(reader["mmp_id"].ToString());
                            makeModelPart.mmp_Make_Name = reader["mmp_Make_Name"].ToString();
                            if (!reader["mmp_make"].ToString().Equals("")) makeModelPart.mmp_Make = Int32.Parse(reader["mmp_Make"].ToString());
                            if (!reader["mmp_Model"].ToString().Equals("")) makeModelPart.mmp_Model = Int32.Parse(reader["mmp_model"].ToString());
                            if (!reader["mmp_part"].ToString().Equals("")) makeModelPart.mmp_part = Int32.Parse(reader["mmp_part"].ToString());
                            if (!reader["mmp_Paint_Color"].ToString().Equals("")) makeModelPart.mmp_Paint_Color = Int32.Parse(reader["mmp_Paint_Color"].ToString());
                            if (!reader["mmp_Paint_Color_Name"].ToString().Equals("")) makeModelPart.mmp_Paint_Color_Name = reader["mmp_Paint_Color_Name"].ToString();
                            if (!reader["mmp_Paint_Type"].ToString().Equals("")) makeModelPart.mmp_Paint_Type = Int32.Parse(reader["mmp_Paint_Type"].ToString());
                            if (!reader["mmp_Paint_Type_Name"].ToString().Equals("")) makeModelPart.mmp_Paint_Type_Name = reader["mmp_Paint_Type_Name"].ToString();
                            if (!reader["mmp_Make_Name"].ToString().Equals("")) makeModelPart.mmp_Make_Name = reader["mmp_Make_Name"].ToString();
                            if (!reader["mmp_Model_Name"].ToString().Equals("")) makeModelPart.mmp_Model_Name = reader["mmp_Model_Name"].ToString();
                            if (!reader["mmp_part_Name"].ToString().Equals("")) makeModelPart.mmp_part_Name = reader["mmp_part_Name"].ToString();

                            makeModelPart.mmp_cre_by_name = reader["mmp_cre_by_name"].ToString();
                            makeModelPart.mmp_cre_date = DateTime.Parse(reader["mmp_cre_date"].ToString());


                        }
                    }
                    reader.Dispose();
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return makeModelPart;
        }

        public IEnumerable<MakeModelPart> getPaintColorsByMakeModelPart(int? cond_Make, int? cond_Model, int cond_part)
        {
            List<MakeModelPart> makeModelParts = new List<MakeModelPart>();

            var conn = db.Database.GetDbConnection();

            conn.Open();
            try
            {
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec getMakeModelPartByAll @mmp_Make='" + cond_Make + "',@mmp_Model='" + cond_Model + "',@mmp_part='" + cond_part + "'";
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            MakeModelPart makeModelPart = new MakeModelPart();
                            makeModelPart.mmp_id = Int32.Parse(reader["mmp_id"].ToString());
                            makeModelPart.mmp_Make_Name = reader["mmp_Make_Name"].ToString();
                            if (!reader["mmp_make"].ToString().Equals("")) makeModelPart.mmp_Make = Int32.Parse(reader["mmp_Make"].ToString());
                            if (!reader["mmp_Model"].ToString().Equals("")) makeModelPart.mmp_Model = Int32.Parse(reader["mmp_model"].ToString());
                            if (!reader["mmp_part"].ToString().Equals("")) makeModelPart.mmp_part = Int32.Parse(reader["mmp_part"].ToString());
                            if (!reader["mmp_Paint_Color"].ToString().Equals("")) makeModelPart.mmp_Paint_Color = Int32.Parse(reader["mmp_Paint_Color"].ToString());
                            if (!reader["mmp_Paint_Color_Name"].ToString().Equals("")) makeModelPart.mmp_Paint_Color_Name = reader["mmp_Paint_Color_Name"].ToString();
                            if (!reader["mmp_Paint_Type"].ToString().Equals("")) makeModelPart.mmp_Paint_Type = Int32.Parse(reader["mmp_Paint_Type"].ToString());
                            if (!reader["mmp_Paint_Type_Name"].ToString().Equals("")) makeModelPart.mmp_Paint_Type_Name = reader["mmp_Paint_Type_Name"].ToString();
                            if (!reader["mmp_Make_Name"].ToString().Equals("")) makeModelPart.mmp_Make_Name = reader["mmp_Make_Name"].ToString();
                            if (!reader["mmp_Model_Name"].ToString().Equals("")) makeModelPart.mmp_Model_Name = reader["mmp_Model_Name"].ToString();
                            if (!reader["mmp_part_Name"].ToString().Equals("")) makeModelPart.mmp_part_Name = reader["mmp_part_Name"].ToString();

                            makeModelPart.mmp_cre_by_name = reader["mmp_cre_by_name"].ToString();
                            makeModelPart.mmp_cre_date = DateTime.Parse(reader["mmp_cre_date"].ToString());

                            makeModelParts.Add(makeModelPart);
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return makeModelParts;
        }

        public void Remove(int id)
        {
            MakeModelPart makeModelPart = db.MakeModelParts.Find(id);
            db.MakeModelParts.Remove(makeModelPart);
            db.SaveChanges();
        }
        public void Update(MakeModelPart makeModelPart)
        {
            Result result = new Result();
            db.MakeModelParts.Update(makeModelPart);
            try
            {
                db.SaveChanges();
                result.Message = "Success";
            }
            catch (DbUpdateException ex)
            {
                SqlException innerException = ex.InnerException.InnerException as SqlException;
                if (innerException != null && (innerException.Number == 2627 || innerException.Number == 2601))
                {
                    result.Message = "All ready Exist !!";

                }
                else
                {
                    result.Message = "All ready Exists !!!";
                }
            }
          
        }
        IEnumerable<MakeModelPart> IMakeModelPart.GetMakeModelParts()
        {
            List<MakeModelPart> makeModelParts = new List<MakeModelPart>();
          
            var conn = db.Database.GetDbConnection();

            conn.Open();
            try
            {
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec getMakeModelPart";
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            MakeModelPart makeModelPart = new MakeModelPart();
                            makeModelPart.mmp_id = Int32.Parse(reader["mmp_id"].ToString());
                            makeModelPart.mmp_Make_Name = reader["mmp_Make_Name"].ToString();
                            if (!reader["mmp_make"].ToString().Equals("")) makeModelPart.mmp_Make = Int32.Parse(reader["mmp_Make"].ToString());
                            if (!reader["mmp_Model"].ToString().Equals("")) makeModelPart.mmp_Model = Int32.Parse(reader["mmp_model"].ToString());
                            if (!reader["mmp_part"].ToString().Equals("")) makeModelPart.mmp_part = Int32.Parse(reader["mmp_part"].ToString());
                            if (!reader["mmp_Paint_Color"].ToString().Equals("")) makeModelPart.mmp_Paint_Color = Int32.Parse(reader["mmp_Paint_Color"].ToString());
                            if (!reader["mmp_Paint_Color_Name"].ToString().Equals("")) makeModelPart.mmp_Paint_Color_Name = reader["mmp_Paint_Color_Name"].ToString();
                            if (!reader["mmp_Paint_Type"].ToString().Equals("")) makeModelPart.mmp_Paint_Type = Int32.Parse(reader["mmp_Paint_Type"].ToString());
                            if (!reader["mmp_Paint_Type_Name"].ToString().Equals("")) makeModelPart.mmp_Paint_Type_Name = reader["mmp_Paint_Type_Name"].ToString();

                            if (!reader["mmp_Make_Name"].ToString().Equals("")) makeModelPart.mmp_Make_Name = reader["mmp_Make_Name"].ToString();
                            if (!reader["mmp_Model_Name"].ToString().Equals("")) makeModelPart.mmp_Model_Name = reader["mmp_Model_Name"].ToString();
                            if (!reader["mmp_part_Name"].ToString().Equals("")) makeModelPart.mmp_part_Name = reader["mmp_part_Name"].ToString();

                            makeModelPart.mmp_cre_by_name = reader["mmp_cre_by_name"].ToString();
                            makeModelPart.mmp_cre_date = DateTime.Parse(reader["mmp_cre_date"].ToString());

                            makeModelParts.Add(makeModelPart);
                        }
                    }
                    reader.Dispose();
                }
            }catch(Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return makeModelParts;
          
        }
    }
}
