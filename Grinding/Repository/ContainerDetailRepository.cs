﻿using Grinding.Models;
using Grinding.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class ContainerDetailRepository : IPalletDetail
    {
        private DBContext db;

        public ContainerDetailRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<ContainerDetail> GetContainerDetails => db.ContainerDetails;

        public Result Add(ContainerDetail containerDetail)
        {
            Trace.WriteLine(containerDetail.cond_Inhouse_no);
            Result result = new Result();
            var conn = db.Database.GetDbConnection();
            try
            {
                
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = @"exec addPalletDetail " + 
                    "@cond_Inhouse_no = '" + containerDetail.cond_Inhouse_no + "',@cond_part ='" + containerDetail.cond_part + "',@cond_step ='" + containerDetail.cond_Step + "',@cond_cre_by='" + containerDetail.cond_cre_by + "',@cond_code='"+containerDetail.cond_code+ "',@cond_quantity='"+ containerDetail.cond_quantity+ "',@cond_make='" + containerDetail.cond_Make + "',@cond_model='" + containerDetail.cond_Model+"'";
                    command.CommandText = query;
                   
                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result.Message = reader[0].ToString();
                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message+" "+ex.InnerException;
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public ContainerDetail GetContainerDetail(int id)
        {
            ContainerDetail containerDetail = db.ContainerDetails.Find(id);
            return containerDetail;
        }

        public void Remove(int id)
        {
            ContainerDetail containerDetail = db.ContainerDetails.Find(id);
            db.ContainerDetails.Remove(containerDetail);
            db.SaveChanges();
        }
        public void Update(ContainerDetail containerDetail)
        {
         
            db.ContainerDetails.Update(containerDetail);
            db.SaveChanges();
        }

      

        public IEnumerable<ContainerDetail> GetContainerDetailsByContainer(String con_code)
        {
            List<ContainerDetail> containerDetails = new List<ContainerDetail>();
            var conn = db.Database.GetDbConnection();
            try
            {

                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = string.Format(@"exec getPalletDetails @con_code='{0}'", con_code);
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ContainerDetail containerDetail = new ContainerDetail();
                            containerDetail.cond_id=int.Parse(reader["cond_id"].ToString());
                            containerDetail.cond_Inhouse_no = reader["cond_Inhouse_no"].ToString();
                            containerDetail.cond_Make = int.Parse(reader["cond_Make"].ToString());
                            containerDetail.cond_Make_Name = reader["cond_Make_Name"].ToString();
                            if(!reader["cond_dep_id"].ToString().Equals("")) containerDetail.cond_dep_id = int.Parse(reader["cond_dep_id"].ToString());
                            containerDetail.cond_dep_name = reader["cond_dep_name"].ToString();
                            containerDetail.cond_part = int.Parse(reader["cond_part"].ToString());
                            containerDetail.cond_part_Name = reader["cond_part_Name"].ToString();
                            containerDetail.cond_Model = int.Parse(reader["cond_Model"].ToString());
                            containerDetail.cond_Model_Name = reader["cond_Model_Name"].ToString();
                            containerDetail.cond_part = int.Parse(reader["cond_part"].ToString());
                            containerDetail.cond_part_Name = reader["cond_part_Name"].ToString();
                            containerDetail.cond_Step = int.Parse(reader["cond_Step"].ToString());
                            containerDetail.cond_Step_Name = reader["cond_Step_Name"].ToString();
                            containerDetail.cond_cre_by = int.Parse(reader["cond_cre_by"].ToString());
                            if(!reader["cond_quantity"].ToString().Equals("")) containerDetail.cond_quantity = decimal.Parse(reader["cond_quantity"].ToString());
                            //containerDetail.cond_cre_by_name = reader["cond_cre_by_name"].ToString();
                            containerDetail.cond_approved_status = reader["cond_approved_status"].ToString();
                            containerDetail.cond_Send_yn = reader["cond_Send_yn"].ToString();
                            containerDetail.cond_code = reader["cond_code"].ToString();
                            containerDetails.Add(containerDetail);


                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
               
            }
            finally {
                conn.Close();
            }
            return containerDetails;

          
        }

        public List<ContainerDetail> getValidationDatail(ContainerDetail containerDetail)
        {

         
            List<ContainerDetail> containerDetails = new List<ContainerDetail>();
            var conn = db.Database.GetDbConnection();
            try
            {

                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = string.Format(@"exec getValidationDatail '{0}',{1}", containerDetail.cond_Inhouse_no, containerDetail.cond_part);
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            containerDetail = new ContainerDetail();
                            containerDetail.cond_id = int.Parse(reader["cond_id"].ToString());
                            containerDetail.cond_Inhouse_no = reader["cond_Inhouse_no"].ToString();
                            if (!reader["cond_dep_id"].ToString().Equals("")) containerDetail.cond_dep_id = int.Parse(reader["cond_dep_id"].ToString());
                            containerDetail.cond_dep_name = reader["cond_dep_name"].ToString();
                            containerDetail.cond_Make = int.Parse(reader["cond_Make"].ToString());
                            containerDetail.cond_Make_Name = reader["cond_Make_Name"].ToString();
                            containerDetail.cond_part = int.Parse(reader["cond_part"].ToString());
                            containerDetail.cond_part_Name = reader["cond_part_Name"].ToString();
                            containerDetail.cond_Model = int.Parse(reader["cond_Model"].ToString());
                            containerDetail.cond_Model_Name = reader["cond_Model_Name"].ToString();
                            containerDetail.cond_part = int.Parse(reader["cond_part"].ToString());
                            containerDetail.cond_part_Name = reader["cond_part_Name"].ToString();
                            containerDetail.cond_Step = int.Parse(reader["cond_Step"].ToString());
                            containerDetail.cond_Step_Name = reader["cond_Step_Name"].ToString();
                            containerDetail.cond_cre_by = int.Parse(reader["cond_cre_by"].ToString());
                            containerDetail.cond_cre_by_name = reader["cond_cre_by_name"].ToString();
                            containerDetail.cond_cre_date = DateTime.Parse(reader["cond_cre_date"].ToString());
                            if (!reader["cond_quantity"].ToString().Equals("")) containerDetail.cond_quantity = decimal.Parse(reader["cond_quantity"].ToString());
                            //containerDetail.cond_cre_by_name = reader["cond_cre_by_name"].ToString();
                            containerDetail.cond_approved_status = reader["cond_approved_status"].ToString();
                            containerDetail.cond_Send_yn = reader["cond_Send_yn"].ToString();
                            containerDetail.cond_code = reader["cond_code"].ToString();
                            containerDetails.Add(containerDetail);


                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return containerDetails;
        }

        public List<ContainerDetail> findContainerDetailForSendQc(ContainerDetail containerDetail)
        {
            List<ContainerDetail> containerDetails = new List<ContainerDetail>();
            var conn = db.Database.GetDbConnection();
            try
            {

                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = string.Format(@"exec getContainerDetailForSendToQc '{0}',{1},{2},{3}", containerDetail.cond_Inhouse_no, containerDetail.cond_part,containerDetail.cond_Make,containerDetail.cond_Model);
                    command.CommandText = query;

                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            containerDetail = new ContainerDetail();
                            containerDetail.cond_id = int.Parse(reader["cond_id"].ToString());
                            containerDetail.cond_Inhouse_no = reader["cond_Inhouse_no"].ToString();
                            if (!reader["cond_dep_id"].ToString().Equals("")) containerDetail.cond_dep_id = int.Parse(reader["cond_dep_id"].ToString());
                            containerDetail.cond_dep_name = reader["cond_dep_name"].ToString();
                            containerDetail.cond_Make = int.Parse(reader["cond_Make"].ToString());
                            containerDetail.cond_Make_Name = reader["cond_Make_Name"].ToString();
                            containerDetail.cond_part = int.Parse(reader["cond_part"].ToString());
                            containerDetail.cond_part_Name = reader["cond_part_Name"].ToString();
                            containerDetail.cond_Model = int.Parse(reader["cond_Model"].ToString());
                            containerDetail.cond_Model_Name = reader["cond_Model_Name"].ToString();
                            containerDetail.cond_part = int.Parse(reader["cond_part"].ToString());
                            containerDetail.cond_part_Name = reader["cond_part_Name"].ToString();
                            containerDetail.cond_Step = int.Parse(reader["cond_Step"].ToString());
                            containerDetail.cond_Step_Name = reader["cond_Step_Name"].ToString();
                            containerDetail.cond_cre_by = int.Parse(reader["cond_cre_by"].ToString());
                            if (!reader["cond_quantity"].ToString().Equals("")) containerDetail.cond_quantity = decimal.Parse(reader["cond_quantity"].ToString());
                            //containerDetail.cond_cre_by_name = reader["cond_cre_by_name"].ToString();
                            containerDetail.cond_approved_status = reader["cond_approved_status"].ToString();
                            containerDetail.cond_Send_yn = reader["cond_Send_yn"].ToString();
                            containerDetail.cond_code = reader["cond_code"].ToString();
                            containerDetails.Add(containerDetail);


                        }
                    }
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return containerDetails;
        }

        public IEnumerable<ContainerDetail> getUnsendedContainerDetails(string con_code)
        {
            IEnumerable<ContainerDetail> icontainerDetails = from u in db.ContainerDetails
                                                             where u.cond_code == con_code
                                                             where u.cond_Send_yn != "Y"
                                                             select u;


            return icontainerDetails;
        }
    }
}
