﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class PaintColorRepository : IPaintColor
    {
        private DBContext db;

        public PaintColorRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<PaintColor> GetPaintColors => from u in db.PaintColors
                                                         join d in db.Users
                                                          on u.pc_cre_by equals d.u_id
                                                         join f in db.MasterData
                                                        on u.pc_type equals f.md_id
                                                         select new PaintColor
                                                         {
                                                             pc_id = u.pc_id,
                                                             pc_name = u.pc_name,
                                                             pc_type = u.pc_type,
                                                             pc_type_name = f.md_name,
                                                             pc_cre_by = u.pc_cre_by,
                                                             pc_cre_by_name = d.u_name + '-' + d.u_full_name,
                                                             pc_cre_date = u.pc_cre_date

                                                         };

     

        public void Add(PaintColor paintColor)
        {
            db.PaintColors.Add(paintColor);
            db.SaveChanges();
        }

        public PaintColor GetPaintColor(int id)
        {
            PaintColor paintColor = db.PaintColors.Find(id);
            return paintColor;
        }

        public PaintColor GetPaintColorByName(string pc_name)
        {

            var query = (from u in db.PaintColors
                        join d in db.Users
                         on u.pc_cre_by equals d.u_id
                        join f in db.MasterData
                       on u.pc_type equals f.md_id
                        where u.pc_name == pc_name
                         select new PaintColor
                        {
                            pc_id = u.pc_id,
                            pc_name = u.pc_name,
                            pc_type = u.pc_type,
                            pc_type_name = f.md_name,
                            pc_cre_by = u.pc_cre_by,
                            pc_cre_by_name = d.u_name + '-' + d.u_full_name,
                            pc_cre_date = u.pc_cre_date

                        }).FirstOrDefault();


            return query;
        }

        public IEnumerable<PaintColor> GetPaintColorsByType(int id)
        {
            var query = from u in db.PaintColors
                        join d in db.Users
                         on u.pc_cre_by equals d.u_id
                        join f in db.MasterData
                       on u.pc_type equals f.md_id
                       where u.pc_type==id
                        select new PaintColor
                        {
                            pc_id = u.pc_id,
                            pc_name = u.pc_name,
                            pc_type=u.pc_type,
                            pc_type_name=f.md_name,
                            pc_cre_by = u.pc_cre_by,
                            pc_cre_by_name = d.u_name + '-' + d.u_full_name,
                            pc_cre_date = u.pc_cre_date

                        };


            return query;
        }

        public void Remove(int id)
        {
            PaintColor paintColor = db.PaintColors.Find(id);
            db.PaintColors.Remove(paintColor);
            db.SaveChanges();
        }

        public void Update(PaintColor paintColor)
        {
            db.PaintColors.Update(paintColor);
            db.SaveChanges();
        }
    }
}
