﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class WorkTimeRepository : IWorkTime
    {
        private DBContext db;

        public WorkTimeRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<WorkTime> GetWorkTimes => db.WorkTime;

        public void Add(WorkTime workTime)
        {
            db.WorkTime.Add(workTime);
            db.SaveChanges();
        }

        public WorkTime GetWorkTime(int id)
        {
            WorkTime workTime = db.WorkTime.Find(id);
            return workTime;
        }

        public IEnumerable<WorkTime> GetWorkTimeByContainer(string con_code)
        {
            var query = from u in db.WorkTime
                        join d in db.Users
                         on u.wrk_cre_by equals d.u_id
                        where u.wrk_con_code== con_code
                        orderby u.wrk_cre_date descending
                        select new WorkTime
                        {
                           wrk_id=u.wrk_id,
                           wrk_start_from = u.wrk_start_from,
                           wrk_end_time = u.wrk_end_time,
                           wrk_emp_id = u.wrk_emp_id,
                           wrk_con_code = u.wrk_con_code,
                           wrk_cre_by = u.wrk_cre_by,
                           wrk_cre_by_name = d.u_name + '-' + d.u_full_name,
                           wrk_cre_date = u.wrk_cre_date

                        };


            return query;
        }

        public WorkTime getLastWorkTimeByContainer(string con_code)
        {
            var query = from u in db.WorkTime
                        join d in db.Users
                         on u.wrk_cre_by equals d.u_id
                        where u.wrk_con_code == con_code
                        where u.wrk_end_time==null
                        orderby u.wrk_cre_date descending
                        select new WorkTime
                        {
                            wrk_id = u.wrk_id,
                            wrk_start_from = u.wrk_start_from,
                            wrk_end_time = u.wrk_end_time,
                            wrk_emp_id = u.wrk_emp_id,
                            wrk_con_code = u.wrk_con_code,
                            wrk_cre_by = u.wrk_cre_by,
                            wrk_cre_by_name = d.u_name + '-' + d.u_full_name,
                            wrk_cre_date = u.wrk_cre_date

                        };


            return query.FirstOrDefault();
        }

        public void Remove(int id)
        {
            WorkTime workTime = db.WorkTime.Find(id);
            db.WorkTime.Remove(workTime);
            db.SaveChanges();
        }

        public void Update(WorkTime workTime)
        {
            db.WorkTime.Update(workTime);
            db.SaveChanges();
        }
     
    }
}
