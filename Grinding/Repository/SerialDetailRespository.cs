﻿using Grinding.Models;
using Grinding.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class SerialDetailRespository : ISerialDetail
    {
        private DBContext db;

        public SerialDetailRespository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<SerialDetail> GetSerialDetails => db.SerialDetails;

        public void Add(SerialDetail serialDetail)
        {
            db.SerialDetails.Add(serialDetail);
            db.SaveChanges();
        }

        public SerialDetail GetSerialDetail(int id)
        {
            SerialDetail serialDetail = db.SerialDetails.Find(id);
            return serialDetail;
        }

        public SerialDetail GetSerialDetailBySerialNumberAndPart(string serialNo, int part)
        {
          
            var serialDetail = (from u in db.SerialDetails
                                         where  u.sd_part == part && u.sd_inhouse_no.Equals(serialNo)
                                select u).FirstOrDefault() ;
            return serialDetail;
        }

        public void Remove(int id)
        {
            SerialDetail serialDetail = db.SerialDetails.Find(id);
            db.SerialDetails.Remove(serialDetail);
            db.SaveChanges();
        }

        public string sendToFinalQc(string sd_inhouse_no, int sd_part)
        {
            string result = "";
                var conn = db.Database.GetDbConnection();
                try
                {

                    conn.Open();
                    using (var command = conn.CreateCommand())
                    {
                        string query = @"exec sendToFinalQc @serialNo = '" + sd_inhouse_no + "',@partId ='" + sd_part + "'";
                        command.CommandText = query;

                        DbDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                            result = reader["message"].ToString();
                            }
                        }
                        reader.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }

                return result;
          
        }

        public void Update(SerialDetail serialDetail)
        {
            db.SerialDetails.Update(serialDetail);
            db.SaveChanges();
        }
    }
}
