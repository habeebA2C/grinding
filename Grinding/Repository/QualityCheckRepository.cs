﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class QualityCheckRepository : IQualityCheck
    {
        private DBContext db;

        public QualityCheckRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<QualityCheck> GetQualityChecks => db.QualityCheck;

        public void Add(QualityCheck qualityCheck)
        {
            db.QualityCheck.Add(qualityCheck);
            db.SaveChanges();
        }

        public QualityCheck GetQualityCheck(int id)
        {
            QualityCheck qualityCheck  = db.QualityCheck.Find(id);
            return qualityCheck;
        }

        public void Remove(int id)
        {
            QualityCheck qualityCheck = db.QualityCheck.Find(id);
            db.QualityCheck.Remove(qualityCheck);
            db.SaveChanges();
        }

        public void Update(QualityCheck qualityCheck)
        {
            db.QualityCheck.Update(qualityCheck);
            db.SaveChanges();
        }
        public IEnumerable<QualityCheck> GeQualityChecksByPallet(string con_code)
        {
            var query = from u in db.QualityCheck
                        join d in db.Users
                         on u.qc_cre_by equals d.u_id
                        where u.qc_con_code == con_code
                        orderby u.qc_cre_date descending
                        select new QualityCheck
                        {
                            qc_id=u.qc_id,
                            qc_con_code=u.qc_con_code,
                            qc_cond_id=u.qc_cond_id,
                            qc_remarks=u.qc_remarks,
                            qc_cre_by=u.qc_cre_by,
                            qc_cre_by_name=d.u_name+'-'+d.u_full_name,
                            qc_cre_date=u.qc_cre_date

                        };
                            

            return query;
        }

        public QualityCheck GeQualityChecksByPalletDetailId(int id)
        {
            var query = from u in db.QualityCheck
                        join d in db.Users
                         on u.qc_cre_by equals d.u_id
                        where u.qc_cond_id == id
                        select new QualityCheck
                        {
                            qc_id = u.qc_id,
                            qc_con_code = u.qc_con_code,
                            qc_cond_id = u.qc_cond_id,
                            qc_reason_id = u.qc_reason_id,
                            qc_remarks = u.qc_remarks,
                            qc_cre_by = u.qc_cre_by,
                            qc_cre_by_name = d.u_name + '-' + d.u_full_name,
                            qc_cre_date = u.qc_cre_date

                        };


            return query.FirstOrDefault();
        }
    }
}
