﻿using Microsoft.EntityFrameworkCore;
using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Grinding.Repository
{
    public class DBContext : DbContext
    {

        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }
        public DbSet<User> Users {get;set;}
        public DbSet<Role> Roles { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowDetail> WorkflowDetails { get; set; }
        public DbSet<WorkflowTracker> workflowTrackers { get; set; }
    
        public DbSet<Documents> Documents { get; set; }

 
        public DbSet<RoleMenu> RoleMenus { get; set; }

        public DbSet<Menu> Menus { get; set; }

        public DbSet<UserDepartment> UserDepartments { get; set; }

        public DbSet<Insight> Insights { get; set; }
        public DbSet<Pallet> Pallets { get; set; }
        public DbSet<Category> Categories { get; set; }
       
        public DbSet<ContainerDetail> ContainerDetails { get; set; }

        public DbSet<WorkTime> WorkTime { get; set; }

        public DbSet<PalletNumber> PalletNumbers { get; set; }

        public DbSet<CategoryDetail> CategoryDetails { get; set; }

        public DbSet<Step> Steps { get; set; }
        public DbSet<QualityCheck> QualityCheck { get; set; }

        public DbSet<Reason> Reasons { get; set; }
        public DbSet<PaintColor> PaintColors { get; set; }
        public DbSet<SerialDetail> SerialDetails { get; set; }

        public DbSet<MakeModelPart> MakeModelParts { get; set; }
        public DbSet<MasterData> MasterData { get; set; }
    }
}
