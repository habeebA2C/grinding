﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class MasterDataRepository : IMasterData
    {
        private DBContext db;

        public MasterDataRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<MasterData> GetMasterDatas => from u in db.MasterData
                                                         join d in db.Users
                                                          on u.md_cre_by equals d.u_id
                                                         select new MasterData
                                                         {
                                                             md_id = u.md_id,
                                                             md_name = u.md_name,
                                                             md_type = u.md_type,
                                                             md_cre_by = u.md_cre_by,
                                                             md_cre_by_name = d.u_name + '-' + d.u_full_name,
                                                             md_cre_date = u.md_cre_date

                                                         };

     

        public void Add(MasterData masterData)
        {
            db.MasterData.Add(masterData);
            db.SaveChanges();
        }

        public MasterData GetMasterData(int id)
        {
            MasterData masterData = db.MasterData.Find(id);
            return masterData;
        }

        public IEnumerable<MasterData> GeMasterDataByType(string md_type)
        {
            var query = from u in db.MasterData
                        join d in db.Users
                         on u.md_cre_by equals d.u_id
                        where u.md_type==md_type
                        select new MasterData
                        {
                            md_id = u.md_id,
                            md_name = u.md_name,
                            md_type=u.md_type,
                            md_cre_by = u.md_cre_by,
                            md_cre_by_name = d.u_name + '-' + d.u_full_name,
                            md_cre_date = u.md_cre_date

                        };


            return query;
        }

        public void Remove(int id)
        {
            MasterData masterData = db.MasterData.Find(id);
            db.MasterData.Remove(masterData);
            db.SaveChanges();
        }

        public void Update(MasterData masterData)
        {
            db.MasterData.Update(masterData);
            db.SaveChanges();
        }

        public MasterData GetMasterDataByName(string md_name)
        {
            var query = (from u in db.MasterData
                        join d in db.Users
                         on u.md_cre_by equals d.u_id
                        where u.md_name == md_name
                        select new MasterData
                        {
                            md_id = u.md_id,
                            md_name = u.md_name,
                            md_type = u.md_type,
                            md_cre_by = u.md_cre_by,
                            md_cre_by_name = d.u_name + '-' + d.u_full_name,
                            md_cre_date = u.md_cre_date

                        }).FirstOrDefault();


            return query;
        }
    }
}
