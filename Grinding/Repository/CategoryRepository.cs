﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class CategoryRepository:ICategory
    {
        private DBContext db;

        public CategoryRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<Category> GetCategories => from u in db.Categories
                                                      join d in db.Users
                                                       on u.cat_cre_by equals d.u_id
                                                      join e in db.Departments
                                                      on u.cat_dep_id equals e.d_id
                                                      select new Category
                                                      {
                                                          cat_id = u.cat_id,
                                                          cat_description = u.cat_description,
                                                          cat_dep_id = u.cat_dep_id,
                                                          cat_dep_description = e.d_description,
                                                          cat_cre_by = u.cat_cre_by,
                                                          cat_cre_by_name = d.u_name + '-' + d.u_full_name,
                                                          cat_cre_date = u.cat_cre_date

                                                      };

     

        public void Add(Category category)
        {
            db.Categories.Add(category);
            db.SaveChanges();
        }

        public Category GetCategory(int id)
        {
            Category Pallet = db.Categories.Find(id);
            return Pallet;
        }

        public IEnumerable<Category> GeCategorysByDocument(int rowid, int doc_id)
        {
            var query = from u in db.Categories
                        join d in db.Users
                         on u.cat_cre_by equals d.u_id
                        join e in db.Departments
                        on u.cat_dep_id equals e.d_id
                        select new Category
                        {
                            cat_id = u.cat_id,
                            cat_description = u.cat_description,
                            cat_dep_id=u.cat_dep_id,
                            cat_dep_description=e.d_description,
                            cat_cre_by = u.cat_cre_by,
                            cat_cre_by_name = d.u_name + '-' + d.u_full_name,
                            cat_cre_date = u.cat_cre_date

                        };


            return query;
        }

        public void Remove(int id)
        {
            Category category = db.Categories.Find(id);
            db.Categories.Remove(category);
            db.SaveChanges();
        }

        public void Update(Category category)
        {
            db.Categories.Update(category);
            db.SaveChanges();
        }

        public IEnumerable<Category> GetCategoriesByDepartment(int dep_id)
        {
            var query = from u in db.Categories
                        join d in db.Users
                         on u.cat_cre_by equals d.u_id
                        join e in db.Departments
                        on u.cat_dep_id equals e.d_id
                        where u.cat_dep_id==dep_id
                        select new Category
                        {
                            cat_id = u.cat_id,
                            cat_description = u.cat_description,
                            cat_dep_id = u.cat_dep_id,
                            cat_dep_description = e.d_description,
                            cat_cre_by = u.cat_cre_by,
                            cat_cre_by_name = d.u_name + '-' + d.u_full_name,
                            cat_cre_date = u.cat_cre_date

                        };


            return query;
        }

        public Category GetCategoryByName(string cat_description)
        {
            var query = (from u in db.Categories
                         join d in db.Users
                          on u.cat_cre_by equals d.u_id
                         join e in db.Departments
                         on u.cat_dep_id equals e.d_id
                         where u.cat_description == cat_description
                         select new Category
                         {
                             cat_id = u.cat_id,
                             cat_description = u.cat_description,
                             cat_dep_id = u.cat_dep_id,
                             cat_dep_description = e.d_description,
                             cat_cre_by = u.cat_cre_by,
                             cat_cre_by_name = d.u_name + '-' + d.u_full_name,
                             cat_cre_date = u.cat_cre_date

                         }).FirstOrDefault();
            return query;
        }
    }
}
