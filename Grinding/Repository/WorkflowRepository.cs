﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class WorkflowRepository : IWorkflow
    {
        private DBContext db;

        public WorkflowRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<Workflow> GetWorkflows => from u in db.Workflows
                                                     join d in db.Users
                                                      on u.w_cre_by equals d.u_id
                                                     select new Workflow
                                                     {
                                                         w_id = u.w_id,
                                                         w_description = u.w_description,
                                                         w_active_yn = u.w_active_yn,
                                                         w_cre_by = u.w_cre_by,
                                                         w_cre_by_name = d.u_name + '-' + d.u_full_name,
                                                         w_cre_date = u.w_cre_date

                                                     };

        public void Add(Workflow workflow)
        {
            db.Workflows.Add(workflow);
            db.SaveChanges();
        }

        public Workflow GetWorkflow(int id)
        {
            Workflow workflow = db.Workflows.Find(id);
            return workflow;
        }

        public void Remove(int id)
        {
            Workflow workflow = db.Workflows.Find(id);
            db.Workflows.Remove(workflow);
            db.SaveChanges();
        }

        public void Update(Workflow workflow1)
        {
            db.Workflows.Update(workflow1);
            db.SaveChanges();
        }
    }
}
