﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class CarryAllDetailRepository: ICarryAllDetail
    {
        private readonly DBContext2 db;

        public CarryAllDetailRepository(DBContext2 _db)
        {
            db = _db;
        }
        public IEnumerable<CarryAllDetail> GetCarryAllDetails => db.PartProducts;

        public CarryAllDetail GetCarryAllDetail(int id)
        {
            CarryAllDetail partProduct = db.PartProducts.Find(id);
            return partProduct;
        }

        public IEnumerable<CarryAllDetail> GetCarryAllDetailByType(int type)
        {
            var query = from u in db.PartProducts
                        where u.Type == type
                        select u;

            return query;
        }

        public IEnumerable<CarryAllDetail> GetCarryAllDetailByTypeAndPArrent(int type, int parrentid)
        {
            var query = from u in db.PartProducts
                        where u.Type == type && u.ParentId == parrentid
                        select u;
            return query;
        }
      
    }
}
