﻿using Grinding.Models;
using Grinding.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Repository
{
    public class PalletNumberRepository:IPalletNumber
    {
        private DBContext db;

        public PalletNumberRepository(DBContext _db)
        {
            db = _db;
        }
        public IEnumerable<PalletNumber> GetPalletNumbers => from u in db.PalletNumbers
                                                             join d in db.Users
                                                              on u.pn_cre_by equals d.u_id
                                                             select new PalletNumber
                                                             {
                                                                 pn_id = u.pn_id,
                                                                 pn_description = u.pn_description,
                                                                 pn_cre_by = u.pn_cre_by,
                                                                 pn_type = u.pn_type,
                                                                 pn_cre_by_name = d.u_name + '-' + d.u_full_name,
                                                                 pn_cre_date = u.pn_cre_date
                                                             };

        public void Add(PalletNumber palletNumber)
        {
            db.PalletNumbers.Add(palletNumber);
            db.SaveChanges();
        }

        public IEnumerable<PalletNumber> getAvailablePalletNumber()
        {
            var query = from u in db.PalletNumbers
                        join d in db.Users
                         on u.pn_cre_by equals d.u_id
                        where u.pn_available_yn!="N"
                        orderby u.pn_id descending
                        select new PalletNumber
                        {
                            pn_id = u.pn_id,
                            pn_description = u.pn_description,
                            pn_cre_by = u.pn_cre_by,
                            pn_type = u.pn_type,
                            pn_cre_by_name = d.u_name + '-' + d.u_full_name,
                            pn_cre_date = u.pn_cre_date
                        };
            return query;
        }

        public PalletNumber GetPalletNumber(int id)
        {
            var query = from u in db.PalletNumbers
                        join d in db.Users
                         on u.pn_cre_by equals d.u_id
                        where u.pn_id==id
                        select new PalletNumber
                        {
                            pn_id = u.pn_id,
                            pn_description = u.pn_description,
                            pn_available_yn=u.pn_available_yn,
                            pn_type=u.pn_type,
                            pn_cre_by = u.pn_cre_by,
                            pn_cre_by_name = d.u_name + '-' + d.u_full_name,
                            pn_cre_date = u.pn_cre_date
                        };
            return query.FirstOrDefault();
        }

        public PalletNumber GetPalletNumberByName(string pn_description)
        {
            var query = from u in db.PalletNumbers
                        join d in db.Users
                         on u.pn_cre_by equals d.u_id
                        where u.pn_description == pn_description
                        select new PalletNumber
                        {
                            pn_id = u.pn_id,
                            pn_description = u.pn_description,
                            pn_available_yn = u.pn_available_yn,
                            pn_type = u.pn_type,
                            pn_cre_by = u.pn_cre_by,
                            pn_cre_by_name = d.u_name + '-' + d.u_full_name,
                            pn_cre_date = u.pn_cre_date
                        };
            return query.FirstOrDefault();
        }

        public IEnumerable<PalletNumber> getPalletNumbersByType(string v)
        {
           var query = from u in db.PalletNumbers
                        join d in db.Users
                         on u.pn_cre_by equals d.u_id
                        where u.pn_available_yn!="N"
                        orderby u.pn_id descending
                        select new PalletNumber
                        {
                            pn_id = u.pn_id,
                            pn_description = u.pn_description,
                            pn_cre_by = u.pn_cre_by,
                            pn_type = u.pn_type,
                            pn_cre_by_name = d.u_name + '-' + d.u_full_name,
                            pn_cre_date = u.pn_cre_date
                        };
            return query;
        }

        public void Remove(int id)
        {
            PalletNumber PalletNumber = db.PalletNumbers.Find(id);
            db.PalletNumbers.Remove(PalletNumber);
            db.SaveChanges();
        }

        public void Update(PalletNumber PalletNumber)
        {
            db.PalletNumbers.Update(PalletNumber);
            db.SaveChanges();
        }
    }
}
