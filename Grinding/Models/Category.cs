﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class Category
    {
        [Key]
        [Display(Name = "Cat Id")]
        public int cat_id { get; set; }

        [Display(Name = "Category")]
        public string cat_description { get; set; }

        [Display(Name = "Department")]
        public int cat_dep_id { get; set; }

        [NotMapped]
        [Display(Name = "Department")]
        public string cat_dep_description { get; set; }

        [Display(Name = "Created By")]
        public int cat_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Created By")]
        public string cat_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime cat_cre_date { get; set; }
    }
}
