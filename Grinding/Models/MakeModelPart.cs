﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class MakeModelPart
    {
        [Key]
        public int mmp_id { get; set; }

        [Display(Name = "Make")]
        public int? mmp_Make { get; set; }

        [NotMapped]
        [Display(Name = "Make")]
        public string? mmp_Make_Name { get; set; }

        [Display(Name = "Model")]
        public int? mmp_Model { get; set; }
        [NotMapped]
        [Display(Name = "Model")]
        public string? mmp_Model_Name { get; set; }

        [Display(Name = "Part")]
        public int? mmp_part { get; set; }
        [NotMapped]
        [Display(Name = "PartName")]
        public string? mmp_part_Name { get; set; }

        [Display(Name = "Paint Type")]
        public int mmp_Paint_Type { get; set; }

        [NotMapped]
        [Display(Name = "Paint Type")]
        public string? mmp_Paint_Type_Name { get; set; }

        [Display(Name = "Paint Color")]
        public int? mmp_Paint_Color { get; set; }

        [NotMapped]
        [Display(Name = "Paint Color Name")]
        public string? mmp_Paint_Color_Name { get; set; }


        [Display(Name = "Created By")]
        public int mmp_cre_by { get; set; }
        [NotMapped]
        [Display(Name = "Created By")]
        public string? mmp_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime mmp_cre_date { get; set; }
    }
}
