﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class Pallet
    {
      

        [Key]
        public int con_id { get; set; }

        [Display(Name = "Department")]
        public int con_dep_id { get; set; }

        [Display(Name = "doc_id")]
        public int con_doc_id { get; set; }

    [NotMapped]
        [Display(Name = "Department")]
        public string con_dep_desciption { get; set; }

        [Display(Name = "workflow")]
        public  int con_workflow_id { get; set; }

        [Display(Name = "Pallet")]
        public string con_code { get; set; }

        [NotMapped]
        [Display(Name = "Pallet")]
        public string? con_code_desc { get; set; }

        [Display(Name = "PalletNumber")]
        public int con_pall_id { get; set; }

        [NotMapped]
        [Display(Name = "PalletNumber")]
        public string con_pall_description { get; set; }

        [Display(Name = "Category")]
        public int con_cat_id { get; set; }

        [NotMapped]
        [Display(Name = "Category")]
        public string con_cat_description { get; set; }

        [Display(Name = "Allocated_to")]
        public int? con_allocated_to { get; set; }

        [NotMapped]
        [Display(Name = "Allocated To")]
        public string con_allocated_to_Name { get; set; }

        [Display(Name = "Allocated By")]
        public int? con_allocated_by { get; set; }

        [NotMapped]
        [Display(Name = "Step")]
        public string con_steps { get; set; }

        [Display(Name = "Created By")]
        public int con_cre_by { get; set; }

        [Display(Name = "Finished Yn")]
        public string? con_finished_yn { get; set; }

        [NotMapped]
        [Display(Name = "No of Products")]
        public int? con_cond_count { get; set; }

        [NotMapped]
        [Display(Name = "worktime In Seconds")]
        public int con_workedTime { get; set; }

        [Display(Name = "Status")]
        internal int? con_status;

        [NotMapped]
        [Display(Name = "Created By")]
        public string con_cre_by_name { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Cre Date")]
        public DateTime con_cre_date { get; set; }
    }
}
