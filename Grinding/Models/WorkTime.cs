﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class WorkTime
    {
        [Key]
        public int wrk_id { get; set; }


        public string wrk_con_code { get; set; }
        public int wrk_emp_id { get; set; }

      
        [Display(Name = "start Time")]
        public DateTime? wrk_start_from { get; set; }

       
        [Display(Name = "End Time")]
        public DateTime? wrk_end_time { get; set; }

        [Display(Name = "Cre by")]
        public int wrk_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Cre by Name")]
        public string wrk_cre_by_name { get; set; }

      
        [Display(Name = "Cre Date")]
        public DateTime wrk_cre_date { get; set; }
    }
}
