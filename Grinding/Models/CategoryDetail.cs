﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class CategoryDetail
    {
        [Key]
        public int catd_id { get; set; }

        public int catd_cat_id { get; set; }
       
        [NotMapped]
        [Display(Name = "Category")]
        public string catd_cat_description { get; set; }

        [Display(Name = "Part")]
        public int? catd_part { get; set; }
       
        [NotMapped]
        [Display(Name = "PartName")]
        public string? catd_part_Name { get; set; }

        [Display(Name = "Make")]
        public int? catd_Make { get; set; }

        [NotMapped]
        [Display(Name = "Make")]
        public string? catd_Make_Name { get; set; }

        [Display(Name = "Model")]
        public int? catd_Model { get; set; }

        [NotMapped]
        [Display(Name = "Model")]
        public string? catd_Model_Name { get; set; }

        [Display(Name = "Type")]
        public int catd_Step { get; set; }

        [NotMapped]
        [Display(Name = "Type")]
        public string? catd_Step_Name { get; set; }

        [Display(Name = "Created By")]
        public int catd_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Created By")]
        public string? catd_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime catd_cre_date { get; set; }
    }
}
