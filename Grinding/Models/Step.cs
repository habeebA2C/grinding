﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class Step
    {
        [Key]
        [Display(Name = "Step Id")]
        public int s_id { get; set; }

        [Display(Name = "Step")]
        public string s_description { get; set;}

        [Display(Name = "Active")]
        public string s_active_yn { get; set; }
        [Display(Name = "Created by")]
        public int s_cre_by { get; set; }
        [NotMapped]
        [Display(Name = "Created by")]
        public string s_cre_by_name { get; set; }

        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        public DateTime s_cre_date { get; set; }
    }
}
