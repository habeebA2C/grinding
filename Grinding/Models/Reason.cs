﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class Reason
    {
        [Key]
        public int rs_id { get; set; }

        [Display(Name = "Reason")]
        public string rs_description { get; set; }

        [Display(Name = "Type")]
        public int rs_type { get; set; }

        [Display(Name = "Cre by")]
        public int rs_cre_by { get; set; }

        [NotMapped]
        [DataType(DataType.Date)]
        [Display(Name = "Cre by Name")]
        public string rs_cre_by_name { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime rs_cre_date { get; set; }
    }
    
}
