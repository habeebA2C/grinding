﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class QualityCheck
    {
        [Key]
        public int qc_id { get; set; }

        [Display(Name = "pallet code")]
        public string qc_con_code { get; set; }

        [Display(Name = "Cond Detail")]
        public int qc_cond_id { get; set; }

        [Display(Name = "Reason Id")]
        public int qc_reason_id { get; set; }

        [NotMapped]
        [Display(Name = "Reason Description")]
        public int qc_reason_description { get; set; }


        [Display(Name = "Qty")]
        public float qc_qty { get; set; }

        [Display(Name = "Remarks")]
        public string qc_remarks { get; set; }

        [Display(Name = "Status")]
        public string qc_status { get; set; }

        [Display(Name = "Cre by")]
        public int qc_cre_by { get; set; }
        [DataType(DataType.Date)]

        [NotMapped]
        [Display(Name = "Cre by Name")]
        public string qc_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime qc_cre_date { get; set; }
    }
}
