﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class ConGrinding
    {
        public int NumberOfDep { get; set; }
        public int NumberOfEmp { get; set; }
        public decimal TotalWorkingHour { get; set; }
    }
}
