﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class Workflow
    {

        [Key]
        public int w_id { get; set; }

        [Display(Name = "Workflow name")]
        public string w_description { get; set; }

        [Display(Name = "Active")]
        public string w_active_yn { get; set; }
        [Display(Name = "Created by")]
        public int w_cre_by { get; set; }
        [NotMapped]
        [Display(Name = "Created By")]
        public string w_cre_by_name { get; set; }

        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        public DateTime w_cre_date { get; set; }
    }
}
