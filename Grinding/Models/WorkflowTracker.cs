﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class WorkflowTracker
    {
        [Key]
        public int wt_id { get; set; }

        [Display(Name = "Pallet Code")]
        public string wt_con_code { get; set; }

        public int wt_pall_id { get; set; }

        [Display(Name = "Pallet Code")]
        public string wt_pall_desc { get; set; }
        public int wt_dep_id { get; set; }
        

        [NotMapped]
        [Display(Name = "Old Department")]
        public string wt_dep_description { get; set; }
        public int wt_dep_to { get; set; }

        [NotMapped]
        [Display(Name = "New Deparment")]
        public string wt_dep_to_description { get; set; }
        public int wt_workflow_id { get; set; }

        public int wt_workflow_to { get; set; }

        [Display(Name = "Serial No")]
        public string wt_serial_no { get; set; }
        [Display(Name = "Role ")]
        public int  wt_cat_id { get; set; }
        public int wt_step_id { get; set; }

        [Display(Name = "Step Completed")]
        public string wt_step_description { get; set; }

        public int wt_step_to { get; set; }
        [Display(Name = "Next Step")]
        public string wt_step_to_description { get; set; }
        public int wt_status { get; set; }
        public int wt_status_to { get; set; }
      

        [Display(Name = "Approved Status")]
        public string wt_approve_status { get; set; }
        [Display(Name = "Cre by")]
        public int wt_cre_by { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Cre by Name")]
        public string wt_cre_by_name { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime wt_cre_date { get; set; }
    }
}
