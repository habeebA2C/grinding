﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class PendingPart
    {
        public int PartId { get; set; }
        public string PartName { get; set; }
        public DateTime CreatedDate { get; set; }
        //public decimal Cost { get; set; }
        //public string inhouse_yn { get; set; }
        //public string qty_change_yn { get; set; }
    }
}
