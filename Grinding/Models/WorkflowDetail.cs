﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class WorkflowDetail
    {
        [Key]
        public int wd_id { get; set; }
        [Display(Name = "workflow")]
        public int wd_workflow_id { get; set; }
        [Display(Name = "Step id")]
        public int wd_step_id { get; set; }
        [NotMapped]
        [Display(Name = "Step")]
        public string wd_step_description { get; set; }
        [Display(Name = "Priority")]
        public int wd_priority { get; set; }
        [Display(Name = "Active")]
        public string wd_active_yn { get; set; }
        [NotMapped]
        [Display(Name = "Created By ")]
        public string wd_cre_by_name { get; set; }
        [Display(Name = "Created by")]
        public int wd_cre_by { get; set; }
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        public DateTime wd_cre_date { get; set; }
    }
}
