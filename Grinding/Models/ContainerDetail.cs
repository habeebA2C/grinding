﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class ContainerDetail
    {
        [Key]
        public int cond_id { get; set; }

        [Display(Name = "Pallet")]
        public string cond_code { get; set; }

        [Display(Name = "Inhouse")]
        public string? cond_Inhouse_no { get; set; }

        [Display(Name = "Department")]
        public int? cond_dep_id { get; set; }

        [NotMapped]
        [Display(Name = "Department")]
        public string? cond_dep_name { get; set; }

        [Display(Name = "Part")]
        public int cond_part { get; set; }
     
        [NotMapped]
        [Display(Name = "PartName")]
        public string? cond_part_Name { get; set; }

        [Display(Name = "Make")]
        public int? cond_Make { get; set; }

        [NotMapped]
        [Display(Name = "Make")]
        public string? cond_Make_Name { get; set; }

        [Display(Name = "Model")]
        public int? cond_Model { get; set; }

        [NotMapped]
        [Display(Name = "Model")]
        public string? cond_Model_Name { get; set; }

        [Display(Name = "Step")]
        public int? cond_Step { get; set; }

        [NotMapped]
        [Display(Name = "Step")]
        public string? cond_Step_Name { get; set; }

        [Column(TypeName = "decimal(18,3)")]
        [Display(Name = "Quantity")]
        public decimal? cond_quantity { get; set; }

        [Display(Name = "Send To Qc")]
        public string cond_Send_yn { get; set; }

        [Display(Name = "Approval Status")]
        public string? cond_approved_status { get; set; }

        [Display(Name = "Cre By")]
        public int cond_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Cre By")]
        public string cond_cre_by_name { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Cre Date")]
        public DateTime cond_cre_date { get; set; }

       
    }
}
