﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class SerialDetail
    {
        [Key]
        public int sd_id { get; set; }

        [Display(Name = "WorkFlow")]
        public int sd_workflow_id { get; set; }

        [Display(Name = "Inhouse Serial No")]
        public string sd_inhouse_no { get; set; }

        [Display(Name = "Make")]
        public int sd_make { get; set; }

        [Display(Name = "Model")]
        public int sd_model { get; set; }

        [Display(Name = "Part")]
        public int sd_part { get; set; }
    
        [Display(Name = "Status")]
        public int sd_status { get; set; }

        [Display(Name = "Status")]
        public string sd_sendto_qc_yn { get; set; }

        [Display(Name = "Cre by")]
        public int sd_cre_by { get; set; }
        [DataType(DataType.Date)]

        [NotMapped]
        [Display(Name = "Cre by Name")]
        public string sd_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime sd_cre_date { get; set; }
    }
}
