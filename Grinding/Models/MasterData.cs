﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class MasterData
    {
        [Key]
        public int md_id { get; set; }

        [Display(Name = "Name")]
        public string md_name { get; set; }

        [Display(Name = "Type")]
        public string md_type { get; set; }

        [Display(Name = "Created By")]
        public int md_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Created By")]
        public string md_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime md_cre_date { get; set; }
    }
}
