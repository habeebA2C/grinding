﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class PaintColor
    {
        [Key]
        public int pc_id { get; set; }

        [Display(Name = "Color")]
        public string pc_name { get; set; }

       
        [Display(Name = "Paint Type")]
        public int pc_type { get; set; }
        [NotMapped]
        [Display(Name = "Paint Type")]
        public string pc_type_name { get; set; }

        [Display(Name = "Created By")]
        public int pc_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Created By")]
        public string pc_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]
        public DateTime pc_cre_date { get; set; }
    }
}
