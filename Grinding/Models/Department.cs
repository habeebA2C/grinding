﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class Department
    {
        [Key]

        public int d_id { get; set; }
        [Display(Name = "Department")]
        public string d_description { get; set; }

        [Display(Name = "WorkFlow")]
        public int d_workflow { get; set; }

        [NotMapped]
        [Display(Name = "WorkFlow ")]
        public string d_workflow_name { get; set; }

        [Display(Name = "Active")]
        public string d_active_yn { get; set; }

        [Display(Name = "Created by")]
        public int d_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Created by")]
        public string d_cre_by_name { get; set; }

        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        public DateTime d_cre_date { get; set; }
    }
}
