﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class PalletNumber
    {
        [Key]
        public int pn_id { get; set; }

        [Display(Name = "Pallet Number")]
        public string pn_description { get; set; }


        [Display(Name = "Available")]
        public string? pn_available_yn { get; set; }

        [Display(Name = "Type")]
        public string? pn_type { get; set; }

        [Display(Name = "Active")]
        public string? pn_active_yn { get; set; }


        [Display(Name = "Created By")]
        public int pn_cre_by { get; set; }

        [NotMapped]
        [Display(Name = "Created By")]
        public string pn_cre_by_name { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Cre Date")]

        public DateTime pn_cre_date { get; set; }
    }
}
