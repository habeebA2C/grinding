﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Models
{
    public class CarryAllDetail
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int ParentId { get; set; }
        public int Type { get; set; }

    }
}
