﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IReason
    {
        IEnumerable<Reason> GetReasons { get; }
        Reason GetReason(int id);

        void Add(Reason reason);

        void Remove(int id);
        void Update(Reason reason);
    }
}
