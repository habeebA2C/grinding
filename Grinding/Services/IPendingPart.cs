﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{

    public interface IPendingPart
    {
        PendingPart GetPendingPart(int id);

        IEnumerable<PendingPart> GetPendingParts { get; }

        IEnumerable<PendingPart> GetParts();



    }

}
