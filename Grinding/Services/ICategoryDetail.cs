﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface ICategoryDetail
    {
        CategoryDetail GetCategoryDetail(int id);

        IEnumerable<CategoryDetail> GetCategoryDetails();

        Result Add(CategoryDetail categoryDetail);

        void Remove(int id);

        void Update(CategoryDetail categoryDetail);

      
    }
}
