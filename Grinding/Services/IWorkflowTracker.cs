﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IWorkflowTracker
    {
        IEnumerable<WorkflowTracker> GetWorkflowTrackers { get; }
        WorkflowTracker GetWorkflowTracker(int id);

        void Add(WorkflowTracker workflowTracker);

        void Remove(int id);
        IEnumerable<WorkflowTracker> GetWorkflowTrackersbySerialNo(string serialNo);
        int getNextPriorityByWorkflowAndSerialNumber(int workflow, string serialNo);
    }
}
