﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IPalletNumber
    {
        PalletNumber GetPalletNumber(int id);

        IEnumerable<PalletNumber> GetPalletNumbers { get; }

        void Add(PalletNumber PalletNumber);

        void Remove(int id);

        void Update(PalletNumber PalletNumber);
        IEnumerable<PalletNumber> getAvailablePalletNumber();
        IEnumerable<PalletNumber> getPalletNumbersByType(string v);
        PalletNumber GetPalletNumberByName(string pn_description);
    }
}
