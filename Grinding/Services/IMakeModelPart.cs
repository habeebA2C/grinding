﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IMakeModelPart
    {
        MakeModelPart GetMakeModelPart(int id);

        IEnumerable<MakeModelPart> GetMakeModelParts();

        Result Add(MakeModelPart makeModelPart);

        void Remove(int id);

        void Update(MakeModelPart makeModelPart);
        MakeModelPart getMakeModelPartByMakeModelPart(int? cond_Make, int? cond_Model, int? cond_part);
        IEnumerable<MakeModelPart> getPaintColorsByMakeModelPart(int? cond_Make, int? cond_Model, int cond_part);
    }
}
