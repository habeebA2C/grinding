﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{

    public interface ICarryAllDetail
    {
        CarryAllDetail GetCarryAllDetail(int id);

        IEnumerable<CarryAllDetail> GetCarryAllDetails { get; }

        IEnumerable<CarryAllDetail> GetCarryAllDetailByType(int type);
        IEnumerable<CarryAllDetail> GetCarryAllDetailByTypeAndPArrent(int type, int parrentid);
    }

}
