﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IStep
    {
        IEnumerable<Step> GetSteps { get; }
        Step GetStep(int id);

        void Add(Step role);

        void Remove(int id);
        void Update(Step step);
        IEnumerable<Step> GetStepsByWorkflow(int workflow );
        Step getStepByName(string s_description);
    }
}
