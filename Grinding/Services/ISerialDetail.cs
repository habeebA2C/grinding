﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface ISerialDetail
    {
        IEnumerable<SerialDetail> GetSerialDetails { get; }
        SerialDetail GetSerialDetail(int id);

        void Add(SerialDetail serialDetail);

        void Remove(int id);
        void Update(SerialDetail serialDetail);
        SerialDetail GetSerialDetailBySerialNumberAndPart(string serialNo, int part);
        string sendToFinalQc(string sd_inhouse_no, int sd_part);
    }
}
