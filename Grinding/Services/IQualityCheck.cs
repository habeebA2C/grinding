﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IQualityCheck
    {
        QualityCheck GetQualityCheck(int id);

        IEnumerable<QualityCheck> GetQualityChecks { get; }

        void Add(QualityCheck qualityCheck);

        void Remove(int id);

        void Update(QualityCheck qualityCheck);

        IEnumerable<QualityCheck> GeQualityChecksByPallet(string con_code);
        QualityCheck GeQualityChecksByPalletDetailId(int id);
    }
}
