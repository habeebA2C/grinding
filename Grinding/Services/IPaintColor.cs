﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IPaintColor
    {
        PaintColor GetPaintColor(int id);

        IEnumerable<PaintColor> GetPaintColors { get; }

        void Add(PaintColor paintColor);

        void Remove(int id);

        void Update(PaintColor paintColor);
        IEnumerable<PaintColor> GetPaintColorsByType(int type);
        PaintColor GetPaintColorByName(string pc_name);
    }
}
