﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IPalletDetail
    {
        ContainerDetail GetContainerDetail(int id);

        IEnumerable<ContainerDetail> GetContainerDetails { get; }

        Result Add(ContainerDetail containerDetail);

        void Remove(int id);

        void Update(ContainerDetail containerDetail);


        IEnumerable<ContainerDetail> GetContainerDetailsByContainer(String con_code);
        List<ContainerDetail> getValidationDatail(ContainerDetail containerDetail);
        List<ContainerDetail> findContainerDetailForSendQc(ContainerDetail containerDetail);
        IEnumerable<ContainerDetail> getUnsendedContainerDetails(string con_code);
    }
}
