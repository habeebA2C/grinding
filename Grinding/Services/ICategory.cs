﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface ICategory
    {
        Category GetCategory(int id);

        IEnumerable<Category> GetCategories { get; }

        void Add(Category category);

        void Remove(int id);

        void Update(Category category);
        IEnumerable<Category> GetCategoriesByDepartment(int dep_id);
        Category GetCategoryByName(string cat_description);
    }
}
