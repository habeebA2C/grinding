﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IWorkTime
    {
        WorkTime GetWorkTime(int id);

        IEnumerable<WorkTime> GetWorkTimes { get; }

        void Add(WorkTime workTime);

        void Remove(int id);

        void Update(WorkTime workTime);

        IEnumerable<WorkTime> GetWorkTimeByContainer(string con_code);
        WorkTime getLastWorkTimeByContainer(string wrk_con_code);
    }
}
