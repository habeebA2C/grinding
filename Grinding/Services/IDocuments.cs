﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IDocuments
    {
        IEnumerable<Documents> GetDocuments { get; }
        Documents GetDocument(int id);

        void Add(Documents documents);

        void Remove(int id);
        void Update(Documents documents);
        public IEnumerable<Documents> GetDocumentsList();

        string GetDocumentNumber(int doc_id);
    }
}
