﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IPallet
    {
        Pallet GetPallet(int id);

        IEnumerable<Pallet> GetPallets { get; }

        Result Add(Pallet Pallet);

        void Remove(int id);

        void Update(Pallet Pallet);

        Pallet GetPalletByCode(string con_code);
        IEnumerable<Pallet> getMyPallets(int u_id);
        IEnumerable<Pallet> GetPalletBySearch(DateTime from, DateTime to, int con_cre_by, int con_pall_id, int con_cat_id);
        IEnumerable<Pallet> PalletReportBySearch(DateTime from, DateTime to, int con_cre_by, int con_pall_id, int con_cat_id,int con_allocated_by,string Type);
        IEnumerable<Pallet> AverageTimeTakenBySearch(DateTime from, DateTime to, int con_cat_id, int con_allocated_by);
        DataTable WorkAllocationBySearch(DateTime from, DateTime to,int dep_id);
        IEnumerable<Pallet> ActivePalletsBySearch(int con_cre_by, int con_pall_id, int con_cat_id, int con_allocated_to);
        IEnumerable<Pallet> findPallet(string serialNo, int step, int part, int make, int model, string type);
       
    }
}
