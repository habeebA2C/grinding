﻿using Grinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grinding.Services
{
    public interface IMasterData
    {
        MasterData GetMasterData(int id);

        IEnumerable<MasterData> GetMasterDatas { get; }

        void Add(MasterData masterData);

        void Remove(int id);

        void Update(MasterData masterData);

        IEnumerable<MasterData> GeMasterDataByType(string type);
        MasterData GetMasterDataByName(string md_name);
    }
}
