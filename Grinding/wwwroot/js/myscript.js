﻿$(document).ready(function () {
   
    $("#rq_cre_by").select2();
    $("#mmp_part").select2();
    $("#mmp_Make").select2();
    $("#mmp_Model").select2();
    $("#mmp_Paint_Color").select2();
    $("#rq_dep_id").select2();
    $("#rq_cre_for").select2();
    $("#catd_part").select2();
    $("#catd_Make").select2();
    $("#Make").select2();
    $("#catd_Model").select2();
    $("#Model").select2();
    $("#con_cre_by").select2();
    $("#con_allocated_by").select2();
    $("#User").select2();
   
    $('#saveMenu').click(function () {
      
        var reqRow = [];
        $("#multiselect_to option").each(function () {
            reqRow.push($(this).val());
        });

        var value = $('#SelectRole').val();
        if (value == 0) {
            launch_toast('Please Choose Role', 'Orange');
            
        } else {
            var type = $('input[name=frmTypes]:checked').val();
            var data = new FormData();

            data.append('role', value);
            data.append('type', type);
            data.append('menus', reqRow);
            $.ajax({
                url: "/RoleMenu/saveMenuitems",
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: data,
                success: function (response) {
                    launch_toast(response, 'green');
                    $("#multiselect_to").empty();
                    $("#multiselect").empty();
                },
                error: function () {
                    $("#multiselect_to").empty();
                    $("#multiselect").empty();

                }
            });
        }
    });

    $("#SelectRole").change(function () {
        load_user_menus();
    });
    $("#multiselect_rightSelected").click(function () {
        if ($("#multiselect_to option[value='" + $("#multiselect").val()+"']").length ==0) {
            $("#multiselect_to").append($("<option />").val($("#multiselect option:selected").val()).text($("#multiselect option:selected").text()));
            $("#multiselect option[value='" + $("#multiselect option:selected").val()+"']").remove();
        }
    });
    $("#multiselect_rightAll").click(function () {
        if ($('#multiselect option').length != 0) {
            $("#multiselect_to").empty();
            $("#multiselect option").each(function () {

                if ($("#multiselect_to option[value='" + $(this).val() + "']").length == 0) {
                    $("#multiselect_to").append($("<option />").val($(this).val()).text($(this).text()));
                    $("#multiselect option[value='" + $(this).val() + "']").remove();
                }
            });
        }    
    });
    $("#multiselect_leftSelected").click(function () {
        if ($("#multiselect option[value='" + $("#multiselect_to").val() + "']").length == 0) {
           
            $("#multiselect").append($("<option />").val($("#multiselect_to option:selected").val()).text($("#multiselect_to option:selected").text()));
            $("#multiselect_to option[value='" + $("#multiselect_to option:selected").val() + "']").remove();
        }
    });
    $("#multiselect_leftAll").click(function () {
        if ($('#multiselect_to option').length != 0) {
            $("#multiselect").empty();
            $("#multiselect_to option").each(function () {

                if ($("#multiselect option[value='" + $(this).val() + "']").length == 0) {
                    $("#multiselect").append($("<option />").val($(this).val()).text($(this).text()));
                    $("#multiselect_to option[value='" + $(this).val() + "']").remove();
                }
            });
        }
    });
    $('.multiselect').multiselect();
    $('#forWhom').change(function() {
        if (!$(this).is(":checked")) {
            var returnVal = confirm("Are you sure?");
            $(this).attr("uncheck", returnVal);
        }
        else {
            alert();
        }
              
    });
    $('[data-toggle="tooltip"]').tooltip(); 
    if ($("#demo").length) {
        setInterval(myTimer, 1000);
    }
    if ($("#startWork").length) {
       // alert();
       setInterval(loadTimeForAllTr, 1000);
       // loadTimeForAllTr();
    }
    if ($("#mytable").length) {
        $('#mytable').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'pdfHtml5'
            ]
         } );
    }
    if ($("#table2").length) {
        $('#table2').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'pdfHtml5'
            ]
        });
    }
    if ($("#con_code").length) {
        if ($("#con_code").val().length > 1) {
            getPallet();
            getPalletDetail();
        }
       
        
    }
   
    $("#name").autocomplete({

        source: function (req, resp) {
            var data = new FormData();
            data.append('name', $("#name").val());
            $.ajax({
                url: "/GrindingRequest/UsersName",
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: data,
                success: function (data) {
                    resp(data);
                },
                error: function () {
                }
            });
        },
        select: function (event, ui) {

            var TABKEY = 9;
            this.value = ui.item.value;

            if (event.keyCode == TABKEY) {
                event.preventDefault();
                $('#name').focus();
            }

            return false;
        },
        position: {
            offset: '1000 4' // Shift 20px to the left, 4px down.
        }
    });
   
    $('input[type=radio][name=frmTypes]').change(function () {

        load_user_menus();
    });
    $('#role').change(function () {
        load_user_menus()
    });
   

});

function openWorkflowDetl(id) {
        var data = new FormData();
        data.append('ID', id);
        $.ajax({
            url: "/Workflow/Details",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                $("#modalContainer").html(response);
                $("#wd_workflow_id").val(id);
                $('#myModal').modal('show');

            },
            error: function () {
            }
        });
}

function saveWorkFlowDetails() {
    var workflowid = $("#wd_workflow_id").val();
    var stepid = $("#wd_step_id").val();
    var priority = $("#wd_priority").val();

    var data = new FormData();
    data.append('wd_workflow_id', workflowid);
    data.append('wd_step_id', stepid);
    data.append('wd_priority', priority);
    $.ajax({
        url: "/WorkflowDetail/Create",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#modalContainer").html(response);
            $('#myModal').modal('show');
            $("#wd_priority").val("");

        },
        error: function () {
        }
    });
}


function GrindingRequestReport() {
    var data = new FormData();
    data.append("rq_dep_id", $("#rq_dep_id").val());
    data.append("reportrange", $("#reportrange").val());
    data.append("rq_cre_by", $("#rq_cre_by").val());
    data.append("rq_cre_date", $("#rq_cre_date").val());
    $.ajax({
        url: "/GrindingRequest/CustomReport",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#Container").html(response);
            $('#mytable').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'pdfHtml5'
                ]
            });
        },
        error: function () {
        }
    });
}

function workflowHistory(rowid,doc_id,workflow,status) {
    var data = new FormData();
    data.append("rowid", rowid);
    data.append("doc_id", doc_id);
    data.append("workflow", workflow);
    
    $.ajax({
        url: "/WorkflowTracker/History",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            workflowDetailStatus(workflow, status);
            $("#modalContainer").html(response);
            $('#historyModal').modal('show');

        },
        error: function () {
        }
    });
}

function workflowDetailStatus( workflow,status) {
    var data = new FormData();
    data.append("workflow", workflow);
    data.append("status", status); 
    $.ajax({
        url: "/WorkflowDetail/Status",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#cont").html(response);
          
        },
        error: function () {
        }
    });
}

function GrindingConsolidatedReport() {
    var data = new FormData();
    data.append("rq_dep_id", $("#rq_dep_id").val());
    data.append("rq_cre_for", $("#rq_cre_for").val());
    data.append("reportrange", $("#reportrange").val());
    $.ajax({
        url: "/GrindingRequest/ConsolidatedReports",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#Container").html(response);
            $('#mytable').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'pdfHtml5'
                ]
            });
        },
        error: function () {
        }
    });
}
$(function () {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});



function myTimer() {
 var date1 = new Date();
  //  var date2 = Date.parse("2020/05/29 21:59:00");
    var date2 = Date.parse($("#lbl_start").text());
var delta = Math.abs(date1 - date2) / 1000;

// calculate (and subtract) whole days
var days = Math.floor(delta / 86400);
delta -= days * 86400;

// calculate (and subtract) whole hours
var hours = Math.floor(delta / 3600) % 24;
delta -= hours * 3600;

// calculate (and subtract) whole minutes
var minutes = Math.floor(delta / 60) % 60;
delta -= minutes * 60;

// what's left is seconds
var seconds = delta % 60;
    document.getElementById("demo").innerHTML = days + ": " + hours + ":" + minutes + ":" + Math.round(seconds);
}


function loadTimeForAllTr() {
 
    if ($("#mytable").length) {
      
        $("tr.a").each(function (i, tr) {
            var value = $(this).find("input.b").val();
         
            var date1 = new Date();
            //  var date2 = Date.parse("2020/05/29 21:59:00");
            var date2 = Date.parse($(this).find("#starttime" + value).val());
          
            if ($(this).find("#EndTime" + value).val() != "") {
                date1 = Date.parse($(this).find("#EndTime" + value).val());
            }
            var delta = Math.abs(date1 - date2) / 1000;

            // calculate (and subtract) whole days
            var days = Math.floor(delta / 86400);
            delta -= days * 86400;

            // calculate (and subtract) whole hours
            var hours = Math.floor(delta / 3600) % 24;
            delta -= hours * 3600;

            // calculate (and subtract) whole minutes
            var minutes = Math.floor(delta / 60) % 60;
            delta -= minutes * 60;

            // what's left is seconds
            var seconds = delta % 60;
            $(this).find("#Duration" + value).text(n(days) + ": " + n(hours) + ":" + n(minutes) + ":" + n(Math.round(seconds)));
        });
    }

}

function n(n) {
    return n > 9 ? "" + n : "0" + n;
}

function holdHistory(rowid, doc_id,from) {
    var data = new FormData();
    data.append("rowid", rowid);
    data.append("doc_id", doc_id);
    data.append("from", from);

    $.ajax({
        url: "/Hold/History",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#modalContainer").html(response);
            $('#historyModal').modal('show');
        },
        error: function () {
        }
    });
}
function replayForHold(id) {
    var data = new FormData();
    data.append("id", id);
    data.append("replay", $("#replay" + id).val().replace(/[\n\r]/g, ''));
    $.ajax({
        url: "/Hold/Replay",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
        },
        error: function () {
        }
    });
}
function hold(id) {
    var data = prompt("Reason For Blocking?? ", "");
    if (data != null) {
        $("#reason" + id).val(data);
        $("#hold" + id).submit();
    }
   
}
function unhold(id) {
    var data = prompt("Reason For Blocking?? ", "");
    if (data != null) {
        $("#reason" + id).val(data);
        $("#unhold" + id).submit();
    }
   
}

function load_user_menus() {
    var $option = $('#SelectRole').find('option:selected');
    var role = $option.val();

    var type = $('input[name=frmTypes]:checked').val();
    var data = new FormData();
    data.append('role', role);
    data.append('type', type);
    $.ajax({
        url: "/RoleMenu/showRoleMenus",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            var data = JSON.parse(response);
            var i;
            if ($.isEmptyObject(response)) {
                $("#multiselect_to").empty();
                $("#multiselect").empty();
            } else {
              
                $('#saveMenu').prop('disabled', false);
                $("#multiselect_to").empty();
                $("#multiselect").empty();
                for (i = 0; i < data.userMenu.length; ++i) {
                    $("#multiselect_to").append($("<option />").val(data.userMenu[i].m_id).text(data.userMenu[i].m_description));
                   
                }
                for (i = 0; i < data.allMenu.length; ++i) {
                    $("#multiselect").append($("<option />").val(data.allMenu[i].m_id).text(data.allMenu[i].m_description));

                }
            }
        },
        error: function () {
            
            $("#multiselect_to").empty();
            $("#multiselect").empty();
        }
    });
}

function viewInsights(id,doc_id,flag) {
    var data = new FormData();
    data.append('id', id);
    data.append('doc_id', doc_id);
    $.ajax({
        url: "/Insights/index",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#insightsContainer").html(response);
            if (flag == 0) {
                $("#filter").empty()
            }
            $("#id").val(id);
            $("#doc_id").val(doc_id);
            $('#insightsModal').modal('show');
        },
        error: function () {
        }
    });
}

function saveInsight() {
    var id = $("#id").val();
    var doc_id = $("#doc_id").val();
    var in_remarks = $("#remarks").val();

    var data = new FormData();
    data.append('id', id);
    data.append('doc_id', doc_id);
    data.append('remarks', in_remarks);
    $.ajax({
        url: "/Insights/Create",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#insightsContainer").html(response);
            $("#id").val(id);
            $("#doc_id").val(doc_id);
            $('#insightsModal').modal('show');
            $("#remarks").val("");

        },
        error: function () {
        }
    });
}

function Reject(id) {
    var data = prompt("Reason For Rejection?? ", "");
    if (data != null) {
        $("#reason" + id).val(data);
        $("#reject" + id).submit();
    }
}

function consolidatedByType() {

    var data = new FormData();
    data.append("type", $("#type").val());
    if ($("#type").val() != "") {
        data.append("reportrange", $("#reportrange").val());
        $.ajax({
            url: "/GrindingRequest/consolidateReportByType",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                $("#Pallet").html(response);

                if ($("#type").val() == "Department") {
                    $('td[name^="emp"]').remove();
                    $('th[name^="emp"]').remove();
                }
                else {
                    $('td[name^="dep"]').remove();
                    $('th[name^="dep"]').remove();
                }
                $('#mytable').DataTable({
                    dom: 'lBfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'pdfHtml5'
                    ]
                });
            },
            error: function () {
            }
        });
    } else {
        launch_toast('Please choose Report type', 'orange');
       
    }
    
}
function getPallet() {
    var con_code = $("#con_code").val();
    if (con_code != "") {
        var data = new FormData();
        data.append('con_code', con_code);
        $.ajax({
            url: "/Pallet/getPallet",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {

                if (response != null) {
                    $("#createdBy").val(response.con_cre_by_name);
                    $("#createdDate").val(response.con_cre_date);
                    $("#Category").val(response.con_cat_id);
                    $("#palletNumber").val(response.con_pall_description);
                    if (response.con_allocated_to != "0") {
                        $("#Allocate").attr("disabled", true);
                        $("#Part").attr("disabled", true);
                        $("#Step").attr("disabled", true);
                        $("#serialNo").attr("disabled", true);
                    } else {
                        $("#Allocate").attr("disabled", false);
                        $("#Part").attr("disabled", false);
                        $("#Step").attr("disabled", false);
                        $("#serialNo").attr("disabled", false);
                    }
                } else {
                    launch_toast('No Pallet Found!!', 'orange');

                    $("#createdBy").val("");
                    $("#createdDate").val("");
                    $("#Category").val(0);
                }
                getPalletDetail();
            },
            error: function () {
            }
        });

    } else {
        launch_toast('Please Enter Pallet', 'orange');
    }
}

function changeParts(type, which, target) {
    var value = $("#" + which).val();
    var data = new FormData();
    data.append('type', type);
    data.append('parrentid', value);
    $.ajax({
        url: "/CarryAllDetail/Parts",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#" + target).empty();
            $("#" + target).append("<option value=''>Select Option</option>");
            if (response.length > 1) {
                for (var i = 1; i < response.length; i++) {

                    $("#" + target).append("<option value=" + response[i].id + ">" + response[i].productName + "</option>");
                    //etc
                }
            }
        },
        error: function () {
        }
    });
}

function addPalletsDetail() {
    var Part = $("#Part").val();
    var Step = $("#Step").val();
    var SerialNo = $("#serialNo").val();
    var cond_code = $("#con_code").val();
    var quantity = $("#quantity").val();
    var data = new FormData();
    if (cond_code != "" && SerialNo != "" && Part!=0)
    {
        data.append('cond_code', cond_code)
        data.append('cond_part', Part);
        data.append('cond_step', Step);
        data.append('cond_Inhouse_no', SerialNo);
        data.append('cond_quantity', quantity);
        $.ajax({
            url: "/Pallet/addPalletDetail",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {

                if (response.message == "Success") {
                    launch_toast('Successfully Added', 'green');
                    getPallet();

                } else {
                    launch_toast(response.message, 'red');
                   
                }
                $("#serialNo").val("");
                setTimeout(function () { $('#serialNo').focus() }, 200);
            },
            error: function () {
            }
        });
    }else
    {
        launch_toast('Please Enter All Data!!', 'red');
       
    }
}
function getPalletDetail() {
    var con_code = $("#con_code").val();
    var data = new FormData();
    data.append('con_code', con_code);
    $.ajax({
        url: "/Pallet/getPalletDetail",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {

            $('#Container').html(response);
           
           
            loadDataTable();
            if ($("#HiddNoOfItems").val() == "0") {
                $("#Allocate").prop("disabled", true);
            }
        },
        error: function () {
        }
    });
}
function loadDataTable() {
    $('#mytable').DataTable({
        order:[],
        dom: 'lBfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5'
        ]
    });
}
function allocatePallet() {
    $("#User").select2();
    $('#UserAllocateModal').modal('show');
  
}

function allocateEmployee() {
    var con_code = $("#con_code").val();
    var User = $("#User").select2().val();
    var data = new FormData();
    data.append('con_code', con_code);
    data.append('allocated_to', User);
    
    if (User != 0 && con_code) {
        $.ajax({
            url: "/Pallet/AllocateUser",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                if (response.message == "Success") {
                    launch_toast("Allocated Successfully", 'green');
                    $('#UserAllocateModal').modal('hide');
                    getPallet(0);
                }
                else {
                    launch_toast(response.message, 'red');

                }
            },
            error: function () {
            }
        });
    } else {
        launch_toast('Please Select Employee', 'red');
    }
}
function startWork() {
    var con_code = $("#con_code").val();
    if (con_code) {
        var data = new FormData();
        data.append('wrk_con_code', con_code);
        $.ajax({
            url: "/WorkTime/Create",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                if (response.message = 'Success') {

                    getWorkTimeByContainer();
                } else {
                    launch_toast(response.message, 'red');
                }
            },
            error: function () {
            }
        });
    }
    else {
        launch_toast('Please Select Pallet !', 'red');
       
    }
  
}
function getWorkTimeByContainer() {
    var con_code = $("#con_code").val();
    if (con_code) {
        var data = new FormData();
        data.append('con_code', con_code);
        $.ajax({
            url: "/WorkTime/getWorkTimeByContainer",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                $("#Container").html(response);
            },
            error: function () {
            }
        });
    } else {
        launch_toast('Please Select Pallet', 'red');
    }
  
}
function FinishWork() {
    var data = new FormData();
    var con_code = $("#con_code").val();
    var data = new FormData();
    data.append('con_code', con_code);
    $.ajax({
        url: "/Pallet/FinishWork",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            if (response.message == "Success") {
                launch_toast('Completed Succefully', 'green');
                location.reload();
            } else {
                launch_toast(response.message, 'red');
               
            }
        },
        error: function () {
        }
    });
}
function workAllocation() {
    var data = new FormData();
    data.append("reportrange", $("#reportrange").val());
    data.append("dep_id", $("#dep_id").val());
    $.ajax({
        url: "/Pallet/WorkAllocationBySearch",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#Container").html(response);
            loadDataTable();
        },
        error: function () {
        }
    });
}
function EndWork(wrk_id) {
    var data = new FormData();
    data.append('wrk_id', wrk_id);
    $.ajax({
        url: "/WorkTime/endWorkTime",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            if (response.message) {
                launch_toast("Finished", 'green');
                getWorkTimeByContainer();
            } else {
                launch_toast(response.message, 'red');
               
            }
        },
        error: function () {
        }
    });
}

function createPallet() {
    var con_dep_id = $("#con_dep_id").val();
    var con_cat_id = $("#con_cat_id").val();
    var con_pall_id = $("#con_pall_id").val();
    if (con_dep_id != 0 && con_cat_id != 0 && con_pall_id != 0) {
        var data = new FormData();
        data.append('con_dep_id', con_dep_id);
        data.append('con_cat_id', con_cat_id);
        data.append('con_pall_id', con_pall_id);
        $.ajax({
            url: "/Pallet/create",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                if (response.message == "Success") {
                    launch_toast("Created Successfully", 'green');
                    $("#Con_Details").html(response.objects);
                    $("#code").val(response.objects);
                    $('#PalletInfoModal').modal('show');

                } else {
                    launch_toast(response.message, 'red');

                    location.reload();
                }
            },
            error: function () {
            }
        });
    } else {
        launch_toast("Please Enter All Data!!", 'red');
    }
}
function closePalletInfoModal() {
    $('#UserAllocateModal').modal('hide');
    location.reload();
}

function getPallets() {
    $('#overlay').fadeIn();
    var data = new FormData();
    data.append("reportrange", $("#reportrange").val());
    data.append("con_pall_id", $("#con_pall_id").val());
    data.append("con_cat_id", $("#con_cat_id").val());
    data.append("con_cre_by", $("#con_cre_by").val());
    data.append("Type", $("#Type").val());
    $.ajax({
        url: "/Pallet/GetPalletBySearch",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#Contianer").html(response);
            loadDataTable();
            $('#overlay').fadeOut()
        },
        error: function () {
            $('#overlay').fadeOut()
        }
    });
}

function getPalletsReport() {
    $('#overlay').fadeIn();
    var data = new FormData();
    data.append("Type", $("#Type").val());
    data.append("con_allocated_to", $("#con_allocated_to").select2().val());
    data.append("reportrange", $("#reportrange").val());
    data.append("con_pall_id", $("#con_pall_id").val());
    data.append("con_cat_id", $("#con_cat_id").val());
    data.append("con_cre_by", $("#con_cre_by").select2().val());
    $.ajax({
        url: "/Pallet/PalletsReportBySearch",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#Container").html(response);
            loadDataTable();
            $('#overlay').fadeOut()
        },
        error: function () {
            $('#overlay').fadeOut()
        }
    });
}
function findToQC() {
     var Part = $('input[name=frmTypes]:checked').val();
    
    var SerialNo = $("#serialNo").val();
    var data = new FormData();
    if ( SerialNo != "" && Part != 0) {
        data.append('cond_part', Part);
        data.append('cond_Inhouse_no', SerialNo);
        $.ajax({
            url: "/Pallet/getValidationData",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                $("#Container").html(response);
                $("#serialNo").val("");
                setTimeout(function () { $('#serialNo').focus() }, 200);
            },
            error: function () {
            }
        });
    } else {
        alert("Please Enter All Data!!");
    }
}
function reject(id, status,dep_id) {
    $("#id").val(id);
    $("#status").val(status);
  //  $('input[name=fromdep]:checked').val(dep_id);
    $('#Reject').modal('toggle');
   // $('#fromdep' + dep_id).
    $('#fromdepRej' + dep_id).prop("checked", true);
   
    
    getNextLevelDetails(status);
}

function getNextLevelDetails(status) {
    $('div[name="palletContainer"]').empty;
    $("#ApprovalButtonGroup label").attr("disabled", false);
    $("#ApprovalButtonGroup :input").attr("disabled", false);
    var data = new FormData();
    var id=$("#id").val();
    var dep_id = $('input[name=fromdep]:checked').val();
    var status = $("#status").val();
    data.append('id', id);
    data.append('dep_id', dep_id);
    data.append('status', status);
    $.ajax({
        url: "/Pallet/getNextLevelDetails",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
           // $('span[name="PaintColor"]').html(response.objects.mmp_Paint_Color_Name);
            $('div[name="palletContainer"]').html(response);
          
            if ($("#NextStep").val() == "Batch Making") {
                $("#ApprovalButtonGroup label").attr("disabled", true);
                $("#ApprovalButtonGroup :input").attr("disabled", true);
            };
        },
        error: function () {
        }
    });
}
function validationModel(id, status, dep_id) {
    $("#id").val(id);
    $("#status").val(status);
  //  $('input[name=fromdep]:checked').val(dep_id);
    // $('#fromdep' + dep_id).button('toggle');
    $('#fromdepApp' + dep_id).prop("checked", true);
   // $('#fromdepApplbl' + dep_id).addClass("active");
   
    $('#Approve').modal('toggle');
    getNextLevelDetails(status);
   
}

function validate(id, status) {
    var document_No = $('input[name=chk_pallet]:checked').val();

        var flag = true;
        var rowid = $("#id").val();
        var status = $("#status").val();
        var ReasonType = $('#ReasonType').val();
        var Remarks = $('#Remarks').val() + " ";
      //  alert($("#NextStep").val());
        var dep_id = $('input[name=fromdep]:checked').val();
          if (status == "Approved") {
              flag = true;
          }
          else if ($("#NextStep").val() == "Batch Making"){
              flag = true;
          }
          else if (status == "Rejected" && ReasonType != 0 && Remarks != "") {
              flag = true;
          }
          else {
              flag = false;
          }
         

          
    if (rowid != 0 && status != "" && (document_No != null || $("#NextStep").val() == "Batch Making") && flag ) {
            var data = new FormData;
            data.append("id", rowid);
            data.append("status", status);
            data.append("ReasonType", ReasonType);
            data.append("Remarks", Remarks);
            data.append("dep_id", dep_id);
            data.append("document_No", document_No);
            $.ajax({
                type: "POST",
                url: '/Pallet/Validate',
                type: "POST",
                contentType: false,
                processData: false,
                data: data,
                cache: false,
                success: function (result) {

                    if (result.message == "Success") {
                        launch_toast("Validation Completed", 'green');
                        $('#tr' + rowid).remove();
                        $("#serialNo").val("");
                        setTimeout(function () { $('#serialNo').focus() }, 200);
                        if (status == "Rejected") {
                            $('#Reject').modal('hide');
                            $('#ReasonType').val(0);
                            $('#Remarks').val("");
                            $("#serialNo").val("");
                            setTimeout(function () { $('#serialNo').focus() }, 200);
                        } else {
                            $('#Approve').modal('hide');
                            $('#Remarks').val("");
                            $("#serialNo").val("");
                            setTimeout(function () { $('#serialNo').focus() }, 200);
                        }
                    } else {
                        alert(result.message);
                        $("#serialNo").val("");
                        setTimeout(function () { $('#serialNo').focus() }, 200);
                    }
                }
            });
        }
        else {
            launch_toast('please Fill All Data', 'orange');
           
        }
    
}
function findContainerDetailForSendQc() {
    var Part =  $('input[name=frmTypes]:checked').val();
    var SerialNo = $("#serialNo").val();
    //var Make = $("#Make").select2().val();
    //var Model = $("#Model").select2().val();
    var Make = 0;
    var Model = 0;
    var data = new FormData();
    if ((SerialNo != "" || (Make != 0 && Model !=0) )&& Part != 0 ) {
        data.append('cond_part', Part);
        data.append('cond_Inhouse_no', SerialNo);
        data.append('cond_Make', Make);
        data.append('cond_Model', Model);
        $.ajax({
            url: "/Pallet/findContainerDetailForSendQc",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                $("#Container").html(response);
                $("#serialNo").val("");
                setTimeout(function () { $('#serialNo').focus() }, 200);
            },
            error: function () {
            }
        });
    } else {
        launch_toast('Please Enter All Data', 'red');
      
    }
}
function SendToQC(id) {
    $('#overlay').fadeIn();
    var data = new FormData();
    data.append("cond_id", id);
    $.ajax({
        url: "/Pallet/UpdateFinishYn",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            if (response.message == "Success") {
                launch_toast('Send To QC SuccessFully', 'green');
                $('#tr' + id).remove();
                $("#serialNo").val("");
                setTimeout(function () { $('#serialNo').focus() }, 200);
            }
            else
            {
                launch_toast(response.message, 'red');
               
            }
        },
        error: function () {
            $('#overlay').fadeOut()
        }
    });
}

function getActivePallets() {
    $('#overlay').fadeIn();
    var data = new FormData();
    data.append("con_allocated_to", $("#con_allocated_to").select2().val());
    data.append("con_pall_id", $("#con_pall_id").val());
    data.append("con_cat_id", $("#con_cat_id").val());
    data.append("con_cre_by", $("#con_cre_by").select2().val());
    $.ajax({
        url: "/Pallet/ActivePalletsBySearch",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#Container").html(response);
            loadDataTable();
            $('#overlay').fadeOut()
        },
        error: function () {
            $('#overlay').fadeOut()
        }
    });
}
function RejectedReason(id) {

    var data = new FormData();
    data.append("id", id);
    $.ajax({
        url: "/Pallet/RejectedReason",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {

            if (response.message == "Success") {
               
                $('#Remarks').val(response.objects.qc_remarks);
               
                $('#ReasonType').val(response.objects.qc_reason_id);
                $('#Reject').modal('toggle');
            }
            else {
                launch_toast(response.message, 'red');
               
            }
           
        },
        error: function () {
        }
    });
}

$('input[type=radio][name=rd_serial_No]').change(function () {
    if (this.value !='WithSRlNo') {
        $("#Make").attr("disabled", false);
        $("#Model").attr("disabled", false);
        $("#Quantity").attr("disabled", false);
        $("#serialNo").attr("disabled", true);
        $("#btn-Allocate").attr("disabled", false);
    } else {
        $("#Make").attr("disabled", true);
        $("#Model").attr("disabled", true);
        $("#Quantity").attr("disabled", true);
        $("#Quantity").val("1");
        $("#Make").val(0).trigger('change.select2');;
        $("#Model").val(0).trigger('change.select2');;
        $("#serialNo").attr("disabled", false);
        $("#btn-Allocate").attr("disabled", true);
    }
});
function findPallet() {
    $('#overlay').fadeIn();
    var Type = $('input[name=rd_serial_No]:checked').val();
    var flag = true;
    if (Type == 'WithSRlNo' && $("#serialNo").val() == null) {
        flag = false;
    }
    if (flag) {
        var data = new FormData();
        data.append("serialNo", $("#serialNo").val());
        data.append("Part", $('input[name=rd_part]:checked').val());
        data.append("Step", $('input[name=rd_step]:checked').val());
        data.append("Make", $("#Make").val());
        data.append("Model", $("#Model").val());
        data.append("Type", Type);
        $.ajax({
            url: "/Pallet/findPallet",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                $("#Container").html(response);
               
                //loadDataTable();
                //$('#overlay').fadeOut()
            },
            error: function () {
                $('#overlay').fadeOut()
            }
        });
    }
    else {
        alert('please scan');
    }
}
function addToPallet(con_code) {
   
    var Part = $('input[name=rd_part]:checked').val();
    var Step = $('input[name=rd_step]:checked').val();
    var Make = $('#Make').select2().val();
    var Model = $('#Model').select2().val();
    var SerialNo = $("#serialNo").val();
    var Quantity = $("#Quantity").val();
   
    var data = new FormData();
    if (con_code != "" && Part != 0) {
        data.append('cond_code', con_code)
        data.append('cond_part', Part);
        data.append('cond_make', Make);
        data.append('cond_model', Model);
        data.append("cond_Step", Step);
        data.append('cond_Inhouse_no', SerialNo);
        data.append('cond_quantity', 1);
        $.ajax({
            url: "/Pallet/addPalletDetail",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                if (response.message == "Success") {
                    launch_toast("Added Successfully", 'green');
                    $("#serialNo").val("");
                    findPallet();
                    setTimeout(function () { $('#serialNo').focus() }, 200);
                } else {
                    alert(response.message);
                    $("#serialNo").val("");
                    findPallet();
                    setTimeout(function () { $('#serialNo').focus() }, 200);
                }
             
            },
            error: function () {
            }
        });
    } else {
        launch_toast("Please Enter All Data", 'red');
        
    }
}


function MovementHistoryBySearch(rowid, doc_id, workflow, status) {
    var serialNo = $("#serialNo").val();
    var data = new FormData();
    data.append("serialNo", serialNo);

    $.ajax({
        url: "/WorkflowTracker/MovementHistoryBySearch",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#container").html(response);
            loadDataTable();
        },
        error: function () {
        }
    });
}


function findPalletForQc() {
    $('#overlay').fadeIn();
    var Type = $('input[name=rd_serial_No]:checked').val();
    var flag = true;
    if (Type == 'WithSRlNo' && $("#serialNo").val() == null) {
        flag = false;
    }
    if (flag) {
        var data = new FormData();
        data.append("serialNo", $("#serialNo").val());
        data.append("Part", $('input[name=rd_part]:checked').val());
        data.append("Step", $('input[name=rd_step]:checked').val());
        data.append("Make", $("#Make").val());
        data.append("Model", $("#Model").val());
        data.append("Type", Type);
        $.ajax({
            url: "/Pallet/findPallet",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            data: data,
            success: function (response) {
                $("#Container").html(response);

                //loadDataTable();
                //$('#overlay').fadeOut()
            },
            error: function () {
                $('#overlay').fadeOut()
            }
        });
    }
    else {
        launch_toast("Please Enter Data", 'red');
     
    }
}

$('input[type=radio][name=fromdep]').change(function () {
    getNextLevelDetails();
});

function categoryByDepartment(which, target) {
    var value = $("#" + which).val();
    var data = new FormData();
    data.append('dep_id', value);
    $.ajax({
        url: "/Category/categoryByDepartment",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#" + target).empty();
            $("#" + target).append("<option value=''>Select Category</option>");
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {

                    $("#" + target).append("<option value=" + response[i].cat_id + ">" + response[i].cat_description + "</option>");
                    //etc
                }
            }
        },
        error: function () {
        }
    });
}


function changePaintType( which, target) {
    var value = $("#" + which).val();
    var data = new FormData();
  
    data.append('type', value);
    $.ajax({
        url: "/PaintColor/getPaintColorByType",
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function (response) {
            $("#" + target).empty();
            $("#" + target).append("<option value=''>Select Option</option>");
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {

                    $("#" + target).append("<option value=" + response[i].pc_id + ">" + response[i].pc_name + "</option>");
                    //etc
                }
            }
        },
        error: function () {
        }
    });
}
function launch_toast(message,color) {
    var x = document.getElementById("toast")
    $("#ToastDescription").html(message);
    $('#toast').css('background-color', color);
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 1500);
}