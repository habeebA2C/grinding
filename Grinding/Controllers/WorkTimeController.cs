﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Grinding.Controllers
{
    public class WorkTimeController : Controller
    {
        private readonly IRole irole;
        private readonly IMenu imenu;
        private readonly IInsight iinsight;
        private readonly IPallet icontainer;
        private readonly ICategory icategory;
        private readonly IDocuments idocuments;
        private readonly IPalletDetail icontainerDetail;
        private readonly IUser iuser;
        private readonly IWorkTime iworkTime;
        public WorkTimeController(IRole _irole, IMenu _imenu, IInsight _iinsight, IPallet _icontainer, ICategory _icategory, IDocuments _idocuments, IPalletDetail _icontainerDetail, IUser _iuser, IWorkTime _iworkTime)
        {
            irole = _irole;
            imenu = _imenu;
            iinsight = _iinsight;
            icontainer = _icontainer;
            icategory = _icategory;
            idocuments = _idocuments;
            icontainerDetail = _icontainerDetail;
            iuser = _iuser;
            iworkTime = _iworkTime;
        }
        // GET: WorkTimeController
        public ActionResult Index()
        {
            return View();
        }

        // GET: WorkTimeController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: WorkTimeController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WorkTimeController/Create
        [HttpPost]
        public Result Create(WorkTime workTime)
        {
            Result result = new Result();
            try
            {
                WorkTime lastWorkTime = iworkTime.getLastWorkTimeByContainer(workTime.wrk_con_code);
                if (lastWorkTime != null)
                {
                    lastWorkTime.wrk_end_time = DateTime.Now;
                    iworkTime.Update(lastWorkTime);

                }
               
                workTime.wrk_start_from = DateTime.Now;
                workTime.wrk_cre_date = DateTime.Now;
                workTime.wrk_cre_by = getCurrentUser().u_id;
                workTime.wrk_emp_id = getCurrentUser().u_id;

                iworkTime.Add(workTime);

                result.Message = "Success";
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message + " " + ex.InnerException;
                return result;
            }
        }

        public Result endWorkTime(int wrk_id)
        {
            Result result = new Result();
            try { 
                WorkTime workTime = iworkTime.GetWorkTime(wrk_id);
                workTime.wrk_end_time = DateTime.Now;
                iworkTime.Update(workTime);
                result.Message = "Success";
            }  
            catch(Exception ex)
            {
                result.Message = ex.Message+ ex.InnerException;
            }
            return result;
        }
        // GET: WorkTimeController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: WorkTimeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult getWorkTimeByContainer(string con_code)
        {
            IEnumerable<WorkTime> workTimes = iworkTime.GetWorkTimeByContainer(con_code);

            return View(workTimes);
        }

        // GET: WorkTimeController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: WorkTimeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        private User getCurrentUser()
        {
            try
            {
                if (HttpContext.Session.GetString("User") == null)
                {
                    return null;
                }
                else
                {
                    User user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("User"));
                    ViewBag.Name = user.u_full_name;
                    ViewBag.isAdmin = user.u_is_admin;

                    List<MenuItems> menulist = new List<MenuItems>();

                    IEnumerable<Menu> menus = imenu.getMenulistByRoleAndType(user.u_role_id, "Menu");

                    foreach (var menu in menus)
                    {
                        MenuItems menuItems = new MenuItems();
                        menuItems.m_id = menu.m_id;
                        menuItems.m_description = menu.m_description;
                        menuItems.m_desc_to_show = menu.m_desc_to_show;
                        menuItems.m_link = menu.m_link;
                        menuItems.m_parrent_id = menu.m_parrent_id;
                        menuItems.m_type = menu.m_type;
                        menuItems.m_cre_by = menu.m_cre_by;
                        menuItems.m_active_yn = menu.m_active_yn;
                        menuItems.m_cre_date = menu.m_cre_date;
                        menuItems.menuItem = imenu.getMenulistByRoleAndTypeAndParrent(user.u_role_id, "MenuItem", menu.m_id);
                        menulist.Add(menuItems);
                    }

                    ViewBag.MenuList = menulist;

                    if (user.u_role_description.Equals("Monitor")) ViewBag.isMonitor = "Y";
                    else
                    {
                        ViewBag.isMonitor = "N";
                    }
                    return user;
                }

            }
            catch
            {
                return null;
            }
        }
    }
}
