﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Grinding.Controllers
{
    public class QualityCheckController : Controller
    {
        private readonly IRole irole;
        private readonly IMenu imenu;
        private readonly IQualityCheck iqualityCheck;
        private readonly ICategory icategory;
        private readonly IUser iuser;
        private readonly IPendingPart ipendingPart;
        private readonly IStep istep;
        private readonly IReason ireason;
        private readonly IDepartment idepartment;
        private readonly IPalletNumber ipalletNumber;
        public QualityCheckController(IRole _irole, IMenu _imenu, IQualityCheck _iqualityCheck, ICategory _icategory, IUser _iuser,IPendingPart _ipendingPart, IStep _istep, IReason _ireason, IPaintColor _ipaintColor, IPalletNumber _ipalletNumber,IDepartment _idepartment)
        {
            irole = _irole;
            imenu = _imenu;
            iqualityCheck = _iqualityCheck;
            icategory = _icategory;
            iuser = _iuser;
            ipendingPart = _ipendingPart;
            istep = _istep;
            ireason = _ireason;
            idepartment = _idepartment;
            ipalletNumber = _ipalletNumber;
        }
        // GET: QualityCheckController
        public ActionResult Index()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Con_code = "";
                ViewBag.Users = iuser.GetUsersList();
                ViewBag.Step = istep.GetSteps;
                ViewBag.Reason = ireason.GetReasons;
                ViewBag.Departments = idepartment.GetDepartments;
                ViewBag.Trolly = ipalletNumber.getPalletNumbersByType("Trolly");
                return View();
            }
        }

        // GET: QualityCheckController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: QualityCheckController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QualityCheckController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: QualityCheckController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: QualityCheckController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: QualityCheckController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: QualityCheckController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        private User getCurrentUser()
        {
            try
            {
                if (HttpContext.Session.GetString("User") == null)
                {
                    return null;
                }
                else
                {
                    User user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("User"));
                    ViewBag.Name = user.u_full_name;
                    ViewBag.isAdmin = user.u_is_admin;

                    List<MenuItems> menulist = new List<MenuItems>();

                    IEnumerable<Menu> menus = imenu.getMenulistByRoleAndType(user.u_role_id, "Menu");

                    foreach (var menu in menus)
                    {
                        MenuItems menuItems = new MenuItems();
                        menuItems.m_id = menu.m_id;
                        menuItems.m_description = menu.m_description;
                        menuItems.m_desc_to_show = menu.m_desc_to_show;
                        menuItems.m_link = menu.m_link;
                        menuItems.m_parrent_id = menu.m_parrent_id;
                        menuItems.m_type = menu.m_type;
                        menuItems.m_cre_by = menu.m_cre_by;
                        menuItems.m_active_yn = menu.m_active_yn;
                        menuItems.m_cre_date = menu.m_cre_date;
                        menuItems.menuItem = imenu.getMenulistByRoleAndTypeAndParrent(user.u_role_id, "MenuItem", menu.m_id);
                        menulist.Add(menuItems);
                    }

                    ViewBag.MenuList = menulist;

                    if (user.u_role_description.Equals("Monitor")) ViewBag.isMonitor = "Y";
                    else
                    {
                        ViewBag.isMonitor = "N";
                    }
                    return user;
                }

            }
            catch
            {
                return null;
            }
        }
    }
}
