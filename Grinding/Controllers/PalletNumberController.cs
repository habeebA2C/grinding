﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Grinding.Controllers
{
    public class PalletNumberController : Controller
    {
        private readonly IRole irole;
        private readonly IMenu imenu;
        private readonly IInsight iinsight;
        private readonly IPalletNumber ipallet;
        private readonly ICategory icategory;
        public PalletNumberController(IRole _irole, IMenu _imenu, IInsight _iinsight, ICategory _icategory, IPalletNumber _ipallet)
        {
            irole = _irole;
            imenu = _imenu;
            iinsight = _iinsight;
            icategory = _icategory;
            ipallet = _ipallet;
        }
        // GET: CategoryController
        public ActionResult Index()
        {

            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(ipallet.GetPalletNumbers);
            }
        }

        // GET: CategoryController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CategoryController/Create
        public ActionResult Create()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View();
            }
        }

        // POST: CategoryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PalletNumber PalletNumber)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    PalletNumber palletNumber1=ipallet.GetPalletNumberByName(PalletNumber.pn_description);
                    if (palletNumber1 == null)
                    {
                        PalletNumber.pn_active_yn = "Y";
                        PalletNumber.pn_available_yn = "Y";
                        PalletNumber.pn_cre_by = getCurrentUser().u_id;
                        PalletNumber.pn_cre_date = DateTime.Now;
                        ipallet.Add(PalletNumber);
                        return RedirectToAction(nameof(Index));
                    }else
                    {
                        ViewBag.Message = "Already Exist!!";
                        return View();
                    }
                }
                catch(Exception ex)
                {
                    ViewBag.Message = ex.Message;
                    return View();
                }
            }
        }

        // GET: CategoryController/Edit/5
        public ActionResult Edit(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(ipallet.GetPalletNumber(id));
            }
        }

        // POST: CategoryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PalletNumber PalletNumber)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    PalletNumber palletNumber1 = ipallet.GetPalletNumberByName(PalletNumber.pn_description);
                    if (palletNumber1 == null)
                    {
                        PalletNumber _pallet = ipallet.GetPalletNumber(id);
                        _pallet.pn_description = PalletNumber.pn_description;
                        _pallet.pn_available_yn = PalletNumber.pn_available_yn;
                        _pallet.pn_type = PalletNumber.pn_type;
                        _pallet.pn_active_yn = PalletNumber.pn_active_yn;
                        ipallet.Update(_pallet);
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Message = "Already Exist!!";
                        return View();
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                    return View();
                }
            }
        }

        
        // GET: CategoryController/Delete/5
        public ActionResult Delete(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(ipallet.GetPalletNumber(id));
            }
        }

        // POST: CategoryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {

                    ipallet.Remove(id);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
        }
        private User getCurrentUser()
        {
            try
            {
                if (HttpContext.Session.GetString("User") == null)
                {
                    return null;
                }
                else
                {
                    User user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("User"));
                    ViewBag.Name = user.u_full_name;
                    ViewBag.isAdmin = user.u_is_admin;

                    List<MenuItems> menulist = new List<MenuItems>();

                    IEnumerable<Menu> menus = imenu.getMenulistByRoleAndType(user.u_role_id, "Menu");

                    foreach (var menu in menus)
                    {
                        MenuItems menuItems = new MenuItems();
                        menuItems.m_id = menu.m_id;
                        menuItems.m_description = menu.m_description;
                        menuItems.m_desc_to_show = menu.m_desc_to_show;
                        menuItems.m_link = menu.m_link;
                        menuItems.m_parrent_id = menu.m_parrent_id;
                        menuItems.m_type = menu.m_type;
                        menuItems.m_cre_by = menu.m_cre_by;
                        menuItems.m_active_yn = menu.m_active_yn;
                        menuItems.m_cre_date = menu.m_cre_date;
                        menuItems.menuItem = imenu.getMenulistByRoleAndTypeAndParrent(user.u_role_id, "MenuItem", menu.m_id);
                        menulist.Add(menuItems);
                    }

                    ViewBag.MenuList = menulist;

                    if (user.u_role_description.Equals("Monitor")) ViewBag.isMonitor = "Y";
                    else
                    {
                        ViewBag.isMonitor = "N";
                    }
                    return user;
                }

            }
            catch
            {
                return null;
            }
        }
    }
}
