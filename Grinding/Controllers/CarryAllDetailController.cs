﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Grinding.Controllers
{
    public class CarryAllDetailController : Controller
    {
        private readonly ICarryAllDetail icarryAllDetail;
        public CarryAllDetailController(ICarryAllDetail _icarryAllDetail)
        {
            icarryAllDetail = _icarryAllDetail;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Parts(int type, int parrentid)
        {
            IEnumerable<CarryAllDetail> carryAllDetails = icarryAllDetail.GetCarryAllDetailByTypeAndPArrent(type, parrentid);
            return Json(carryAllDetails);
        }

    }
}
