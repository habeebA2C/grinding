﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Grinding.Controllers
{
    public class PalletController : Controller
    {
        private readonly IRole irole;
        private readonly IMenu imenu;
        private readonly IInsight iinsight;
        private readonly IPallet ipallet;
        private readonly ICategory icategory;
        private readonly IDocuments idocuments;
        private readonly IPalletDetail  icontainerDetail;
        private readonly IUser  iuser;
        private readonly IWorkTime  iworkTime;
        private readonly ICarryAllDetail icarryAllDetail;
        private readonly IPendingPart ipendingPart;
        private readonly IPalletNumber ipalletNumber;
        private readonly IStep istep;
        private readonly IQualityCheck iqualityCheck;
        private readonly IReason ireason;
        private readonly ISerialDetail iserialDetail;
        private readonly IWorkflowDetail iworkflowDetail;
        private readonly IDepartment idepartment;
        private readonly IWorkflowTracker iworkflowTracker;
        private readonly IMakeModelPart imakeModelPart;

        public PalletController(IRole _irole,IPallet _ipallet,IMenu _imenu, IInsight _iinsight, IPallet _icontainer,ICategory _icategory, IDocuments _idocuments, IPalletDetail _icontainerDetail,IUser _iuser, IWorkTime _iworkTime, ICarryAllDetail _icarryAllDetail, IPendingPart _ipendingPart, IPalletNumber _ipalletNumber, IStep _istep,IQualityCheck _iqualityCheck,IReason _ireason, ISerialDetail _iserialDetail,IWorkflowDetail _iworkflowDetail, IDepartment _idepartment, IWorkflowTracker _iworkflowTracker, IMakeModelPart _imakeModelPart)
        {
            irole = _irole;
            imenu = _imenu;
            iinsight = _iinsight;
            ipallet = _ipallet;
            icategory = _icategory;
            idocuments = _idocuments;
            icontainerDetail = _icontainerDetail;
            iuser = _iuser;
            iworkTime = _iworkTime;
            icarryAllDetail = _icarryAllDetail;
            ipendingPart = _ipendingPart;
            ipalletNumber = _ipalletNumber;
            istep = _istep;
            iqualityCheck=_iqualityCheck;
            ireason=_ireason;
            iserialDetail=_iserialDetail;
            iworkflowDetail = _iworkflowDetail;
            idepartment = _idepartment;
            iworkflowTracker = _iworkflowTracker;
            imakeModelPart = _imakeModelPart;
        }
        // GET: ContainerController
        public ActionResult Index()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.Pallets = ipalletNumber.getAvailablePalletNumber();
                ViewBag.UserList = iuser.GetUsersList();
                return View();
            }
        }


        public ActionResult GetPalletBySearch(string reportrange, int con_cre_by, int con_pall_id,int con_cat_id,int con_allocated_by,string Type)
        {
            String[] array = reportrange.Split('-');

            DateTime from = DateTime.Parse(array[0]);
            DateTime to = DateTime.Parse(array[1] + " 11:59:59 PM");


            IEnumerable<Pallet> containers= ipallet.PalletReportBySearch(from, to,  con_cre_by,  con_pall_id, con_cat_id, con_allocated_by, Type);

            return View(containers);
        }
        // GET: ContainerController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ContainerController/Create
        public ActionResult Create()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {

                ViewBag.Category = icategory.GetCategories;
                ViewBag.Department =  idepartment.GetDepartments;
                ViewBag.PalletNumber = ipalletNumber.getAvailablePalletNumber();
                return View();
            }
        }

        public ActionResult Sorting()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {

                ViewBag.Category = icategory.GetCategories;
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Users = iuser.GetUsersList();
                ViewBag.Step = istep.GetSteps;
                ViewBag.Reason = ireason.GetReasons;
                ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                return View();
            }
        }

        // POST: ContainerController/Create
        [HttpPost]
     
        public Result Create(Pallet pallet)
        {
            Result result = new Result();
            try
            {
                
                string doc_no = idocuments.GetDocumentNumber(1);
                Department department = idepartment.GetDepartment(pallet.con_dep_id);
                pallet.con_doc_id = 1;
                pallet.con_workflow_id = department.d_workflow;
                pallet.con_status = 1;
                pallet.con_code = doc_no;
                pallet.con_allocated_to = 0;
                pallet.con_cre_by = getCurrentUser().u_id;
                pallet.con_cre_date = DateTime.Now;
                result = ipallet.Add(pallet);
                PalletNumber PalletNumber = ipalletNumber.GetPalletNumber(pallet.con_pall_id);
                if (PalletNumber != null)
                {
                    PalletNumber.pn_available_yn = "N";
                    ipalletNumber.Update(PalletNumber);
                    result.Message = "Success";
                }
                else
                {
                    result.Message = " Not Found  !!";
                }
                           

            }
            catch(Exception ex)
            {
                result.Message = ex.Message + " " + ex.InnerException;
            }
            return result;
        }

        // GET: ContainerController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ContainerController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ContainerController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ContainerController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult PalletDetail(string code)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Con_code = code;
                ViewBag.Users = iuser.GetUsersList();
                ViewBag.Step = istep.GetSteps;
                ViewBag.Reason = ireason.GetReasons;
                ViewBag.AllocationAccess= imenu.getMenulistByRoleAndType(getCurrentUser().u_role_id, "Options").Count();
                return View();
            }
        }
        [HttpGet]
        public ActionResult PalletDetail()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
             
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Con_code = "";
                ViewBag.Users = iuser.GetUsersList();
                ViewBag.Step = istep.GetSteps;
                ViewBag.Reason = ireason.GetReasons;
                ViewBag.AllocationAccess = imenu.getMenulistByRoleAndType(getCurrentUser().u_role_id, "Options").Count();
                return View();
            }
        }
        public Result RejectedReason(int id)
        {

            Result result = new Result();
            try
            {
                result.Objects = iqualityCheck.GeQualityChecksByPalletDetailId(id);
                result.Message = "Success";
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }
        public async Task<Result> addPalletDetail(ContainerDetail containerDetail)
        {
            Result result = new Result();
            try
            {
               
                containerDetail.cond_cre_by = getCurrentUser().u_id;
                containerDetail.cond_cre_date = DateTime.Now;
                result=icontainerDetail.Add(containerDetail);
               
                return result;
            }
            catch(Exception ex)
            {
                result.Message = ex.Message+" "+ex.InnerException ;
            }
            return result;


        }
        public ActionResult getValidationData(ContainerDetail containerDetail)
        {
            List<ContainerDetail> pallets=icontainerDetail.getValidationDatail(containerDetail);
         
            return View(pallets);
        }
        public ActionResult SendToQc()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                return View();
            }
        }

        [HttpPost]
        public Result UpdateFinishYn(int cond_id)
        {
            Result result = new Result();
            try
            {
                ContainerDetail containerDetail = icontainerDetail.GetContainerDetail(cond_id);
                Pallet pallet = ipallet.GetPalletByCode(containerDetail.cond_code);
                int time=iworkTime.GetWorkTimeByContainer(containerDetail.cond_code).Count();
                if (time >= 1)
                {
                    containerDetail.cond_Send_yn = "Y";
                    icontainerDetail.Update(containerDetail);
                    result.Message = "Success";
                }else
                {
                    result.Message = "Work Not Started !!";
                }
               

            }catch (Exception ex)
            {
                Trace.WriteLine(ex.InnerException);
                result.Message = ex.Message + " " + ex.InnerException;
            }

            return result;
        }

        public ActionResult findContainerDetailForSendQc(ContainerDetail containerDetail)
        {
            List<ContainerDetail> pallets = icontainerDetail.findContainerDetailForSendQc(containerDetail);
          

            return View(pallets);
        }
        public Result Validate(int id,string status,int reasonType,string Remarks,int dep_id,string document_No)
        {
            Result result = new Result();
           
            try
            {
                Department department = idepartment.GetDepartment(dep_id);
                ContainerDetail containerDetail = icontainerDetail.GetContainerDetail(id);
                Pallet pallet = ipallet.GetPalletByCode(containerDetail.cond_code);
                SerialDetail serialDetail =iserialDetail.GetSerialDetailBySerialNumberAndPart(containerDetail.cond_Inhouse_no, containerDetail.cond_part);
             
                int workflow_old = serialDetail.sd_workflow_id;
                int status_old = serialDetail.sd_status;
                WorkflowDetail workflowDetail_old = iworkflowDetail.getWorkflowDetlByWorkflowCodeAndPriority(workflow_old, status_old);
                Step step = istep.GetStep(workflowDetail_old.wd_step_id);
                Trace.WriteLine(document_No);
                if (status.Equals("Approved") && pallet.con_dep_id == dep_id && step.s_description.Equals("Lacquer"))
                {

                    ViewBag.NextStep = "Batch Making";
                    result.Message= iserialDetail.sendToFinalQc(serialDetail.sd_inhouse_no,serialDetail.sd_part);
                    serialDetail.sd_status = 0;
                    serialDetail.sd_workflow_id =0;
                    serialDetail.sd_sendto_qc_yn = "Y";
                    QualityCheck qualityCheck = new QualityCheck();
                    qualityCheck.qc_cond_id = containerDetail.cond_id;
                    qualityCheck.qc_remarks = Remarks;
                    qualityCheck.qc_status = status;
                    qualityCheck.qc_reason_id = reasonType;
                    qualityCheck.qc_con_code = containerDetail.cond_code;
                    qualityCheck.qc_cre_date = DateTime.Now;
                    qualityCheck.qc_cre_by = getCurrentUser().u_id;
                    iqualityCheck.Add(qualityCheck);

                    WorkflowTracker workflowTracker = new WorkflowTracker();
                  if (containerDetail.cond_code.Equals(""))  workflowTracker.wt_con_code = containerDetail.cond_code;
                    workflowTracker.wt_pall_desc = pallet.con_pall_description;
                    workflowTracker.wt_pall_id = pallet.con_pall_id;
                    workflowTracker.wt_serial_no = serialDetail.sd_inhouse_no;
                    workflowTracker.wt_cat_id = pallet.con_cat_id;
                    workflowTracker.wt_dep_id = pallet.con_dep_id;
                    workflowTracker.wt_dep_to = dep_id;
                    workflowTracker.wt_workflow_id = workflow_old;
                    workflowTracker.wt_workflow_to = serialDetail.sd_workflow_id;
                    workflowTracker.wt_status = status_old;
                    workflowTracker.wt_status_to = serialDetail.sd_status;
                    workflowTracker.wt_step_id = workflowDetail_old.wd_step_id;
                    workflowTracker.wt_step_description = step.s_description;
                    workflowTracker.wt_step_to_description = "Batch Making";
                    workflowTracker.wt_step_to = 9999;
                    workflowTracker.wt_approve_status = status;
                    workflowTracker.wt_cre_by = getCurrentUser().u_id;
                    workflowTracker.wt_cre_by_name = getCurrentUser().u_name + " " + getCurrentUser().u_full_name;
                    workflowTracker.wt_cre_date = DateTime.Now;
                    iworkflowTracker.Add(workflowTracker);
                    iserialDetail.Update(serialDetail);
                    containerDetail.cond_approved_status = status;
                    icontainerDetail.Update(containerDetail);
                    result.Message = "Success";


                }
                else if(!document_No.Equals("undefined")|| !document_No.Equals(""))
                {
                    if (status.Equals("Approved") && pallet.con_dep_id == dep_id)
                    {
                        serialDetail.sd_status = iworkflowDetail.getNextWorkflow(serialDetail.sd_workflow_id, serialDetail.sd_status);
                    }
                    else if (status.Equals("Reject") && pallet.con_dep_id == dep_id)
                    {

                    }
                    else
                    {

                        int nextworkflow = iworkflowTracker.getNextPriorityByWorkflowAndSerialNumber(department.d_workflow, containerDetail.cond_Inhouse_no);
                        if (nextworkflow == 0) nextworkflow = iworkflowDetail.getMinOfWorkFlow(department.d_workflow);
                        serialDetail.sd_workflow_id = department.d_workflow;
                        serialDetail.sd_status = nextworkflow;
                    }

                  
                    WorkflowDetail workflowDetail_new = iworkflowDetail.getWorkflowDetlByWorkflowCodeAndPriority(serialDetail.sd_workflow_id, serialDetail.sd_status);

                   
                    Step step_new = istep.GetStep(workflowDetail_new.wd_step_id);
        
                    ContainerDetail containerDetail1 = new ContainerDetail();
                    containerDetail1.cond_code = document_No;
                    containerDetail1.cond_part = containerDetail.cond_part;
                    containerDetail1.cond_Step = step_new.s_id;
                    containerDetail1.cond_Inhouse_no = containerDetail.cond_Inhouse_no;
                    containerDetail1.cond_quantity = containerDetail.cond_quantity;

                    result = icontainerDetail.Add(containerDetail1);

                    if (result.Message == "Success")
                    {
                        QualityCheck qualityCheck = new QualityCheck();
                        qualityCheck.qc_cond_id = containerDetail.cond_id;
                        qualityCheck.qc_remarks = Remarks;
                        qualityCheck.qc_status = status;
                        qualityCheck.qc_reason_id = reasonType;
                        qualityCheck.qc_con_code = containerDetail.cond_code;
                        qualityCheck.qc_cre_date = DateTime.Now;
                        qualityCheck.qc_cre_by = getCurrentUser().u_id;
                        iqualityCheck.Add(qualityCheck);

                        WorkflowTracker workflowTracker = new WorkflowTracker();
                        workflowTracker.wt_con_code = containerDetail.cond_code;
                        workflowTracker.wt_pall_desc = pallet.con_pall_description;
                        workflowTracker.wt_pall_id = pallet.con_pall_id;
                        workflowTracker.wt_serial_no = serialDetail.sd_inhouse_no;
                        workflowTracker.wt_cat_id = pallet.con_cat_id;
                        workflowTracker.wt_dep_id = pallet.con_dep_id;
                        workflowTracker.wt_dep_to = dep_id;
                        workflowTracker.wt_workflow_id = workflow_old;
                        workflowTracker.wt_workflow_to = serialDetail.sd_workflow_id;
                        workflowTracker.wt_status = status_old;
                        workflowTracker.wt_status_to = serialDetail.sd_status;
                        workflowTracker.wt_step_id = workflowDetail_old.wd_step_id;
                        workflowTracker.wt_step_description = step.s_description;
                        workflowTracker.wt_step_to_description = step_new.s_description;
                        workflowTracker.wt_step_to = workflowDetail_new.wd_step_id;
                        workflowTracker.wt_approve_status = status;
                        workflowTracker.wt_cre_by = getCurrentUser().u_id;
                        workflowTracker.wt_cre_by_name = getCurrentUser().u_name + " " + getCurrentUser().u_full_name;
                        workflowTracker.wt_cre_date = DateTime.Now;
                        iworkflowTracker.Add(workflowTracker);
                        iserialDetail.Update(serialDetail);
                        containerDetail.cond_approved_status = status;
                        icontainerDetail.Update(containerDetail);
                        result.Message = "Success";
                    }
                }else
                {
                    result.Message = "Please Choose Pallet !!";
                }


            }
            catch (Exception ex)
            {
              
                result.Message = ex.Message;
            }

            return result;
        }
        public ActionResult getPallet(string con_code )
        {
            Pallet pallet = ipallet.GetPalletByCode(con_code);
            if(pallet!=null) pallet.con_cond_count = icontainerDetail.GetContainerDetailsByContainer(con_code).Count();
          
            return new JsonResult(pallet);
        }

        public ActionResult getPalletDetail(string con_code)
        {
            IEnumerable<ContainerDetail> containerDetails = icontainerDetail.GetContainerDetailsByContainer(con_code);
            ViewBag.Count = containerDetails.Count();
            return View(containerDetails);
        }
        [HttpPost]
        public ActionResult getNextLevelDetails(int id,int dep_id,string status)
        {
            IEnumerable<Pallet> containers = Enumerable.Empty<Pallet>();
            try
            {
                ContainerDetail containerDetail = icontainerDetail.GetContainerDetail(id);
                MakeModelPart makeModelPart = imakeModelPart.getMakeModelPartByMakeModelPart(containerDetail.cond_Make, containerDetail.cond_Model, containerDetail.cond_part);
                ViewBag.PaintColor = imakeModelPart.getPaintColorsByMakeModelPart(containerDetail.cond_Make, containerDetail.cond_Model, containerDetail.cond_part);

                int make = Convert.ToInt32(containerDetail.cond_Make);
                int model = Convert.ToInt32(containerDetail.cond_Model);

                WorkflowDetail workflowDetail = new WorkflowDetail();

                Pallet pallet = ipallet.GetPalletByCode(containerDetail.cond_code);
                SerialDetail serialDetail = iserialDetail.GetSerialDetailBySerialNumberAndPart(containerDetail.cond_Inhouse_no, containerDetail.cond_part);
                int workflow_old = serialDetail.sd_workflow_id;
                int status_old = serialDetail.sd_status;

                int workflow_new = serialDetail.sd_workflow_id;
                int status_new = serialDetail.sd_status;
                Department department = idepartment.GetDepartment(dep_id);
                WorkflowDetail workflowDetail_old = iworkflowDetail.getWorkflowDetlByWorkflowCodeAndPriority(workflow_old, status_old);
                Step step = istep.GetStep(workflowDetail_old.wd_step_id);
                if (status.Equals("Approved") && pallet.con_dep_id == dep_id && step.s_description.Equals("Lacquer"))
                {

                    ViewBag.NextStep = "Batch Making";
                }
                else
                {
                    if (status.Equals("Approved") && pallet.con_dep_id == dep_id)
                    {
                        status_new = iworkflowDetail.getNextWorkflow(serialDetail.sd_workflow_id, serialDetail.sd_status);


                    }
                    else if (status.Equals("Reject") && pallet.con_dep_id == dep_id)
                    {

                    }
                    else
                    {
                        Trace.WriteLine(dep_id + "  " + status);

                        //Trace.WriteLine(department.d_workflow + department.d_description);
                        //int nextworkflow = iworkflowDetail.getMinOfWorkFlow(department.d_workflow);
                        int nextworkflow = iworkflowTracker.getNextPriorityByWorkflowAndSerialNumber(department.d_workflow, containerDetail.cond_Inhouse_no);
                        if (nextworkflow == 0) nextworkflow = iworkflowDetail.getMinOfWorkFlow(department.d_workflow);
                        Trace.WriteLine(dep_id + "  " + status + "  " + nextworkflow);
                        workflow_new = department.d_workflow;
                        status_new = nextworkflow;
                    }

                   // WorkflowDetail workflowDetail_old = iworkflowDetail.getWorkflowDetlByWorkflowCodeAndPriority(workflow_old, status_old);
                    WorkflowDetail workflowDetail_new = iworkflowDetail.getWorkflowDetlByWorkflowCodeAndPriority(workflow_new, status_new);
                    if (workflowDetail_new != null)
                    {
                       // Step step = istep.GetStep(workflowDetail_old.wd_step_id);
                        Step step_new = istep.GetStep(workflowDetail_new.wd_step_id);
                        ViewBag.NextStep = step_new.s_description;
                        containers = ipallet.findPallet(containerDetail.cond_Inhouse_no, step_new.s_id, containerDetail.cond_part, make, model, "");
                    }
                    else
                    {
                        ViewBag.NextStep = "Choose Next Department!!";
                    }
                }
                return View(containers);
            }catch(Exception ex)
            {
                return View(containers);
            }
        }
        [HttpPost]
        public Result allocateUser(string con_code,int allocated_to)
        {
            Result result = new Result();
            try
            {
                IEnumerable<ContainerDetail> containerDetails = icontainerDetail.GetContainerDetailsByContainer(con_code);
                if (containerDetails.Count() != 0)
                {
                    Pallet Pallet = ipallet.GetPalletByCode(con_code);
                    Pallet.con_allocated_to = allocated_to;
                    Pallet.con_allocated_by = getCurrentUser().u_id;
                    ipallet.Update(Pallet);
                    result.Message = "Success";
                }
                else
                {
                    result.Message = "Pallet is Empty!!";
                }
              
            }catch(Exception ex)
            {
                result.Message = ex.Message+ ' '+ex.InnerException;
            }
            return result;
        }
        public ActionResult StartWork()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Pallet = ipallet.getMyPallets(getCurrentUser().u_id);
                return View();
            }
        }

       
        [HttpPost]
        public Result FinishWork(string con_code)
        {
            Result result = new Result();
            try
            {
                WorkTime lastWorkTime = iworkTime.getLastWorkTimeByContainer(con_code);
               
                IEnumerable<ContainerDetail> containerDetails = icontainerDetail.getUnsendedContainerDetails(con_code);
                
                if (containerDetails.Count() == 0)
                {
                   
                    Pallet Pallet = ipallet.GetPalletByCode(con_code);
                    PalletNumber PalletNumber = ipalletNumber.GetPalletNumber(Pallet.con_pall_id);
                    PalletNumber.pn_available_yn = "Y";
                    ipalletNumber.Update(PalletNumber);
                    Pallet.con_finished_yn = "Y";
                    ipallet.Update(Pallet);
                    if (lastWorkTime != null)
                    {
                        lastWorkTime.wrk_end_time = DateTime.Now;
                        iworkTime.Update(lastWorkTime);

                    }
                    result.Message = "Success";
                }else
                {
                    result.Message = "Unsend SerialNumbers are Exist!!";
                }
              
            }
            catch (Exception ex)
            {
                result.Message = ex.Message+ " "+ex.InnerException;
            }

            return result;
        }

    
        private User getCurrentUser()
        {
            try
            {
                if (HttpContext.Session.GetString("User") == null)
                {
                    return null;
                }
                else
                {
                    User user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("User"));
                    ViewBag.Name = user.u_full_name;
                    ViewBag.isAdmin = user.u_is_admin;

                    List<MenuItems> menulist = new List<MenuItems>();

                    IEnumerable<Menu> menus = imenu.getMenulistByRoleAndType(user.u_role_id, "Menu");

                    foreach (var menu in menus)
                    {
                        MenuItems menuItems = new MenuItems();
                        menuItems.m_id = menu.m_id;
                        menuItems.m_description = menu.m_description;
                        menuItems.m_desc_to_show = menu.m_desc_to_show;
                        menuItems.m_link = menu.m_link;
                        menuItems.m_parrent_id = menu.m_parrent_id;
                        menuItems.m_type = menu.m_type;
                        menuItems.m_cre_by = menu.m_cre_by;
                        menuItems.m_active_yn = menu.m_active_yn;
                        menuItems.m_cre_date = menu.m_cre_date;
                        menuItems.menuItem = imenu.getMenulistByRoleAndTypeAndParrent(user.u_role_id, "MenuItem", menu.m_id);
                        menulist.Add(menuItems);
                    }

                    ViewBag.MenuList = menulist;

                
                    return user;
                }

            }
            catch
            {
                return null;
            }
        }

        public ActionResult PalletReport()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.PalletNumber = ipalletNumber.GetPalletNumbers;
                ViewBag.UserList = iuser.GetUsersList();
                return View();
            }
        }


        public ActionResult PalletsReportBySearch(string reportrange, int con_cre_by, int con_pall_id, int con_cat_id, int con_allocated_to)
        {
            String[] array = reportrange.Split('-');

            DateTime from = DateTime.Parse(array[0]);
            DateTime to = DateTime.Parse(array[1] + " 11:59:59 PM");

            string Type = "";
            IEnumerable<Pallet> containers = ipallet.PalletReportBySearch(from, to, con_cre_by, con_pall_id, con_cat_id, con_allocated_to, Type);

            return View(containers);
        }


        public ActionResult ActivePallets()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.PalletNumber = ipalletNumber.getAvailablePalletNumber();
                ViewBag.UserList = iuser.GetUsersList();
                return View();
            }
        }


        public ActionResult ActivePalletsBySearch( int con_cre_by, int con_pall_id, int con_cat_id, int con_allocated_to)
        {   

            IEnumerable<Pallet> containers = ipallet.ActivePalletsBySearch(con_cre_by, con_pall_id, con_cat_id, con_allocated_to);

            return View(containers);
        }

        public ActionResult findPallet( string serialNo, int Step, int Part, int Make,int  Model,string Type)
        {
            WorkflowDetail workflowDetail = new WorkflowDetail();
            Department department = idepartment.GetDepartment(1);
            int priority = iworkflowDetail.getMinOfWorkFlow(department.d_workflow);

            SerialDetail serialDetail = iserialDetail.GetSerialDetailBySerialNumberAndPart(serialNo, Part);
            
            if (serialDetail != null)
            {
                priority = serialDetail.sd_status;
                ViewBag.Step = istep.GetStepsByWorkflow(serialDetail.sd_workflow_id);
                workflowDetail= iworkflowDetail.getWorkflowDetlByWorkflowCodeAndPriority(serialDetail.sd_workflow_id, priority);
            }
            else
            {
                ViewBag.Step = istep.GetStepsByWorkflow(department.d_workflow);
                workflowDetail= iworkflowDetail.getWorkflowDetlByWorkflowCodeAndPriority(department.d_workflow, priority);

            }
           
            Step step = istep.GetStep(workflowDetail.wd_step_id);
            Step = step.s_id;
            ViewBag.CurrStep = Step;
            IEnumerable<Pallet> containers = ipallet.findPallet(serialNo, Step, Part, Make, Model, Type);
           
            return View(containers);
        }




        public ActionResult workAllocation()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Department = idepartment.GetDepartments;
                return View();
            }
        }


        public ActionResult WorkAllocationBySearch(string reportrange,int dep_id)
        {
            String[] array = reportrange.Split('-');

            DateTime from = DateTime.Parse(array[0]);
            DateTime to = DateTime.Parse(array[1] + " 11:59:59 PM");


            DataTable dt = ipallet.WorkAllocationBySearch(from, to, dep_id);
            if (dt.Rows.Count > 1)
                ViewBag.last = dt.Rows[dt.Rows.Count - 1];
            return View(dt);
        }

        public ActionResult AverageTimeTaken()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.PalletsNumber = ipalletNumber.getAvailablePalletNumber();
                ViewBag.UserList = iuser.GetUsersList();
                return View();
            }
        }


        public ActionResult AverageTimeTakenBySearch(string reportrange,int con_cat_id, int con_allocated_by)
        {
            String[] array = reportrange.Split('-');

            DateTime from = DateTime.Parse(array[0]);
            DateTime to = DateTime.Parse(array[1] + " 11:59:59 PM");


            IEnumerable<Pallet> pallets = ipallet.AverageTimeTakenBySearch(from, to, con_cat_id, con_allocated_by);

            return View(pallets);
        }



    }
}
