﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace Grinding.Controllers
{
    public class PaintColorController : Controller
    {
        public readonly IRole irole;
        private readonly IMenu imenu;
        private readonly IInsight iinsight;
        private readonly IPaintColor ipaintColor;
        private readonly IMasterData imasterData;
    
        public PaintColorController(IRole _irole, IMenu _imenu, IInsight _iinsight, IPaintColor _ipaintColor, IMasterData _imasterData)
        {
            irole = _irole;
            imenu = _imenu;
            iinsight = _iinsight;
            ipaintColor = _ipaintColor;
            imasterData = _imasterData;
          
        }
        // GET: CategoryController
        public ActionResult Index()
        {

            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(ipaintColor.GetPaintColors);
            }
        }

        // GET: CategoryController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CategoryController/Create
        public ActionResult Create()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {

                ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                return View();
            }
        }

        // POST: CategoryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PaintColor paintColor)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    PaintColor paintColor1 = ipaintColor.GetPaintColorByName(paintColor.pc_name);
                    if (paintColor1 == null)
                    {
                        paintColor.pc_cre_by = getCurrentUser().u_id;
                        paintColor.pc_cre_date = DateTime.Now;
                        ipaintColor.Add(paintColor);
                        return RedirectToAction(nameof(Index));
                    }else
                    {
                        ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                        ViewBag.Message = "Already Exist!!";
                        return View();
                    }
                   
                }
                catch(Exception ex)
                {
                    ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                    ViewBag.Message = ex.Message;
                    return View();
                }
            }
        }

        // GET: CategoryController/Edit/5
        public ActionResult Edit(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                return View(ipaintColor.GetPaintColor(id));
            }
        }

        // POST: CategoryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PaintColor paintColor)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    
                     PaintColor _paintColor = ipaintColor.GetPaintColor(id);
                    _paintColor.pc_name = paintColor.pc_name;
                    _paintColor.pc_type = paintColor.pc_type;
                    ipaintColor.Update(_paintColor);
                    return RedirectToAction(nameof(Index));
                   
                }
                catch (Exception ex)
                {
                    ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                    ViewBag.Message = ex.Message;
                    return View();
                }
            }
        }

        // GET: CategoryController/Delete/5
        public ActionResult Delete(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                return View(ipaintColor.GetPaintColor(id));
            }
        }

        // POST: CategoryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {

                    ipaintColor.Remove(id);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
        }
        [HttpPost]
        public JsonResult getPaintColorByType(int type)
        {
            IEnumerable<PaintColor> paintColors = ipaintColor.GetPaintColorsByType(type);
            return Json(paintColors);
        }
        private User getCurrentUser()
        {
            try
            {
                if (HttpContext.Session.GetString("User") == null)
                {
                    return null;
                }
                else
                {
                    User user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("User"));
                    ViewBag.Name = user.u_full_name;
                    ViewBag.isAdmin = user.u_is_admin;

                    List<MenuItems> menulist = new List<MenuItems>();

                    IEnumerable<Menu> menus = imenu.getMenulistByRoleAndType(user.u_role_id, "Menu");

                    foreach (var menu in menus)
                    {
                        MenuItems menuItems = new MenuItems();
                        menuItems.m_id = menu.m_id;
                        menuItems.m_description = menu.m_description;
                        menuItems.m_desc_to_show = menu.m_desc_to_show;
                        menuItems.m_link = menu.m_link;
                        menuItems.m_parrent_id = menu.m_parrent_id;
                        menuItems.m_type = menu.m_type;
                        menuItems.m_cre_by = menu.m_cre_by;
                        menuItems.m_active_yn = menu.m_active_yn;
                        menuItems.m_cre_date = menu.m_cre_date;
                        menuItems.menuItem = imenu.getMenulistByRoleAndTypeAndParrent(user.u_role_id, "MenuItem", menu.m_id);
                        menulist.Add(menuItems);
                    }

                    ViewBag.MenuList = menulist;

                    if (user.u_role_description.Equals("Monitor")) ViewBag.isMonitor = "Y";
                    else
                    {
                        ViewBag.isMonitor = "N";
                    }
                    return user;
                }

            }
            catch
            {
                return null;
            }
        }
    }
}
