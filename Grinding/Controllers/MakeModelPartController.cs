﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Grinding.Controllers
{
    public class MakeModelPartController : Controller
    {
        public readonly IRole irole;
        private readonly IMenu imenu;
        private readonly IMakeModelPart imakeModelPart;
        private readonly ICategory icategory;
        private readonly ICategoryDetail icategoryDetail;
        private readonly IPendingPart ipendingPart;
        private readonly ICarryAllDetail icarryAllDetail;
        private readonly IPaintColor ipaintColor;
        private readonly IPalletDetail ipalletDetail;
        private readonly IMasterData imasterData;

        public MakeModelPartController(IRole _irole, IMenu _imenu, IMakeModelPart _imakeModelPart, ICategory _icategory, ICategoryDetail _icategoryDetail, IPendingPart _ipendingPart, ICarryAllDetail _icarryAllDetail, IPaintColor _ipaintColor, IPalletDetail _ipalletDetail, IMasterData _imasterData)
        {
            irole = _irole;
            imenu = _imenu;
            imakeModelPart = _imakeModelPart;
            icategory = _icategory;
            icategoryDetail = _icategoryDetail;
            ipendingPart = _ipendingPart;
            icarryAllDetail = _icarryAllDetail;
            ipaintColor = _ipaintColor;
            ipalletDetail = _ipalletDetail;
            imasterData = _imasterData;

        }
        // GET: MakeModelPart
        public ActionResult Index()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(imakeModelPart.GetMakeModelParts());
            }
        }

        // GET: MakeModelPart/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MakeModelPart/Create
        public ActionResult Create()
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
             

                return View();
            }
        }

        // POST: MakeModelPart/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MakeModelPart makeModelPart)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {

                    imakeModelPart.getMakeModelPartByMakeModelPart(makeModelPart.mmp_part, makeModelPart.mmp_Make, makeModelPart.mmp_Model);
                   
                    makeModelPart.mmp_cre_by = getCurrentUser().u_id;
                    makeModelPart.mmp_cre_date = DateTime.Now;
                    Result result= imakeModelPart.Add(makeModelPart);
                    if (result.Message.Equals("Success"))
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Error = result.Message;
                        ViewBag.Category = icategory.GetCategories;
                        ViewBag.Part = ipendingPart.GetParts();
                        ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                        ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                        ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                    
                        return View();
                    }
                    
                }
                
                catch (Exception  exc)
                {
                    ViewBag.Category = icategory.GetCategories;
                    ViewBag.Part = ipendingPart.GetParts();
                    ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                    ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                    ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                  
                    return View();
                }
            }
        }

        // GET: MakeModelPart/Edit/5
        public ActionResult Edit(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                ViewBag.PaintColor = ipaintColor.GetPaintColors;
                return View(imakeModelPart.GetMakeModelPart(id));
            }
        }

        // POST: MakeModelPart/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, MakeModelPart makeModelPart)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    MakeModelPart _makeModelPart = imakeModelPart.GetMakeModelPart(id);
                    _makeModelPart.mmp_Make = makeModelPart.mmp_Make;
                    _makeModelPart.mmp_Model = makeModelPart.mmp_Model;
                    _makeModelPart.mmp_part = makeModelPart.mmp_part;
                    _makeModelPart.mmp_Paint_Color = makeModelPart.mmp_Paint_Color;
                    _makeModelPart.mmp_Paint_Type = makeModelPart.mmp_Paint_Type;
                    
                    imakeModelPart.Update(_makeModelPart);
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    ViewBag.Category = icategory.GetCategories;
                    ViewBag.Part = ipendingPart.GetParts();
                    ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                    ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                    ViewBag.PaintType = imasterData.GeMasterDataByType("PaintType");
                    ViewBag.PaintColor = ipaintColor.GetPaintColors;
                    return View();
                }
            }
        }


        [HttpPost]
        public Result getColor(int id)
        {
            Result result = new Result();
            try
            {
               
                ContainerDetail containerDetail = ipalletDetail.GetContainerDetail(id);
                MakeModelPart makeModelPart = imakeModelPart.getMakeModelPartByMakeModelPart(containerDetail.cond_Make, containerDetail.cond_Model, containerDetail.cond_part);
                IEnumerable<MakeModelPart> makeModelParts = imakeModelPart.getPaintColorsByMakeModelPart(containerDetail.cond_Make, containerDetail.cond_Model, containerDetail.cond_part);
                result.Objects = makeModelParts;
                result.Message = "Success";
            }catch(Exception ex)
            {
                result.Message = ex.Message + " " + ex.InnerException;
            }
            return result;
        } 
        
        public ActionResult Delete(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(imakeModelPart.GetMakeModelPart(id));
            }
        }

        // POST: MakeModelPart/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            
            try
            {
                imakeModelPart.Remove(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
               
                return View();
            }
        }
        private User getCurrentUser()
        {
            try
            {
                if (HttpContext.Session.GetString("User") == null)
                {
                    return null;
                }
                else
                {
                    User user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("User"));
                    ViewBag.Name = user.u_full_name;
                    ViewBag.isAdmin = user.u_is_admin;

                    List<MenuItems> menulist = new List<MenuItems>();

                    IEnumerable<Menu> menus = imenu.getMenulistByRoleAndType(user.u_role_id, "Menu");

                    foreach (var menu in menus)
                    {
                        MenuItems menuItems = new MenuItems();
                        menuItems.m_id = menu.m_id;
                        menuItems.m_description = menu.m_description;
                        menuItems.m_desc_to_show = menu.m_desc_to_show;
                        menuItems.m_link = menu.m_link;
                        menuItems.m_parrent_id = menu.m_parrent_id;
                        menuItems.m_type = menu.m_type;
                        menuItems.m_cre_by = menu.m_cre_by;
                        menuItems.m_active_yn = menu.m_active_yn;
                        menuItems.m_cre_date = menu.m_cre_date;
                        menuItems.menuItem = imenu.getMenulistByRoleAndTypeAndParrent(user.u_role_id, "MenuItem", menu.m_id);
                        menulist.Add(menuItems);
                    }

                    ViewBag.MenuList = menulist;

                    if (user.u_role_description.Equals("Monitor")) ViewBag.isMonitor = "Y";
                    else
                    {
                        ViewBag.isMonitor = "N";
                    }
                    return user;
                }

            }
            catch
            {
                return null;
            }
        }
    }
}
