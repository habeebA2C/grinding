﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Grinding.Models;
using Grinding.Repository;
using Grinding.Services;

namespace Grinding.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUser iuser;
        public LoginController(IUser _iuser)
        {
            iuser = _iuser;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
            var key = "shdfg2323g3g4j3879sdfh2j3237w8eh";
            try
            {
                if (user.u_name!=null&&user.u_password!=null)
                {
                    
                  
                    User newuser = iuser.getUserbyUsername(user.u_name);
                    if (newuser != null)
                    {
                        var newPassword = AesOperaions.DecryptString(key, newuser.u_password);

                        if (user.u_password.ToString().Equals(newPassword.ToString()))
                        {
                            newuser.u_password = null;
                            string JsonStr = JsonConvert.SerializeObject(newuser);
                            HttpContext.Session.SetString("User", JsonStr);
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            ViewBag.Message = "Username And Password Are Incorrect!!!";
                            return View("Index");
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Username And Password Are Incorrect!!!";
                        return View("Index");
                    }
                }else
                {
                    ViewBag.Message = "Please Enter Both Username And Password!!!";
                    return View("Index");
                }
                
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return View("Index");
            }
        }
    }
}