﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grinding.Models;
using Grinding.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace Grinding.Controllers
{
    public class CategoryDetailController : Controller
    {
        public readonly IRole irole;
        private readonly IMenu imenu;
        private readonly IInsight iinsight;
        private readonly ICategory icategory;
        private readonly ICategoryDetail icategoryDetail;
        private readonly IPendingPart ipendingPart;
        private readonly ICarryAllDetail icarryAllDetail;
        private readonly IStep istep;
        private readonly IDepartment idepartment;
        public CategoryDetailController(IRole _irole, IMenu _imenu, IInsight _iinsight, ICategory _icategory, ICategoryDetail _icategoryDetail, IPendingPart _ipendingPart, ICarryAllDetail _icarryAllDetail, IStep _istep,IDepartment _idepartment)
        {
            irole = _irole;
            imenu = _imenu;
            iinsight = _iinsight;
            icategory = _icategory;
            icategoryDetail = _icategoryDetail;
            ipendingPart = _ipendingPart;
            icarryAllDetail = _icarryAllDetail;
            istep = _istep;
            idepartment = _idepartment;
        }
        // GET: CategoryController
        public ActionResult Index()
        {

            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(icategoryDetail.GetCategoryDetails());
            }
        }

        // GET: CategoryController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CategoryController/Create
        public ActionResult Create()
        {
            if (getCurrentUser() == null)
            {

                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                ViewBag.Step = istep.GetSteps;

                return View();
            }
        }

        // POST: CategoryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryDetail categoryDetail)
        {
            if (getCurrentUser() == null)
            {

                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    categoryDetail.catd_cre_by = getCurrentUser().u_id;
                    categoryDetail.catd_cre_date = DateTime.Now;
                    icategoryDetail.Add(categoryDetail);
                    return RedirectToAction(nameof(Index));
                }
                catch(Exception ex)
                {
                    ViewBag.Category = icategory.GetCategories;
                    ViewBag.Part = ipendingPart.GetParts();
                    return View();
                }
            }
        }

        // GET: CategoryController/Edit/5
        public ActionResult Edit(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Category = icategory.GetCategories;
                ViewBag.Part = ipendingPart.GetParts();
                ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                ViewBag.Step = istep.GetSteps;
                return View(icategoryDetail.GetCategoryDetail(id));
            }
        }

        // POST: CategoryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CategoryDetail categoryDetail)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    CategoryDetail _categoryDetail = icategoryDetail.GetCategoryDetail(id);
                    _categoryDetail.catd_Make = categoryDetail.catd_Make;
                    _categoryDetail.catd_cat_id = categoryDetail.catd_cat_id;
                    _categoryDetail.catd_part = categoryDetail.catd_part;
                    _categoryDetail.catd_Model = categoryDetail.catd_Model;
                    _categoryDetail.catd_Step = categoryDetail.catd_Step;
                    icategoryDetail.Update(_categoryDetail);
                    return RedirectToAction(nameof(Index));
                }
                catch(Exception ex)
                {
                    ViewBag.Category = icategory.GetCategories;
                    ViewBag.Part = ipendingPart.GetParts();
                    ViewBag.Make = (icarryAllDetail.GetCarryAllDetailByType(1));
                    ViewBag.Model = (icarryAllDetail.GetCarryAllDetailByType(2));
                    ViewBag.Step = istep.GetSteps;
                    ViewBag.Message = ex.Message + " " + ex.InnerException;
                    return View(icategoryDetail.GetCategoryDetail(id));
                }
            }
        }

        // GET: CategoryController/Delete/5
        public ActionResult Delete(int id)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View(icategoryDetail.GetCategoryDetail(id));
            }
        }

     

        // POST: CategoryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            if (getCurrentUser() == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {

                    icategoryDetail.Remove(id);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
        }
        private User getCurrentUser()
        {
            try
            {
                if (HttpContext.Session.GetString("User") == null)
                {
                    return null;
                }
                else
                {
                    User user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("User"));
                    ViewBag.Name = user.u_full_name;
                    ViewBag.isAdmin = user.u_is_admin;

                    List<MenuItems> menulist = new List<MenuItems>();

                    IEnumerable<Menu> menus = imenu.getMenulistByRoleAndType(user.u_role_id, "Menu");

                    foreach (var menu in menus)
                    {
                        MenuItems menuItems = new MenuItems();
                        menuItems.m_id = menu.m_id;
                        menuItems.m_description = menu.m_description;
                        menuItems.m_desc_to_show = menu.m_desc_to_show;
                        menuItems.m_link = menu.m_link;
                        menuItems.m_parrent_id = menu.m_parrent_id;
                        menuItems.m_type = menu.m_type;
                        menuItems.m_cre_by = menu.m_cre_by;
                        menuItems.m_active_yn = menu.m_active_yn;
                        menuItems.m_cre_date = menu.m_cre_date;
                        menuItems.menuItem = imenu.getMenulistByRoleAndTypeAndParrent(user.u_role_id, "MenuItem", menu.m_id);
                        menulist.Add(menuItems);
                    }

                    ViewBag.MenuList = menulist;

                    if (user.u_role_description.Equals("Monitor")) ViewBag.isMonitor = "Y";
                    else
                    {
                        ViewBag.isMonitor = "N";
                    }
                    return user;
                }

            }
            catch
            {
                return null;
            }
        }
    }
}
